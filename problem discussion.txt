problem.. cannot use different weight values for the same exercises, but different routines

so if in routine A i squat heavier than in routine B.... this cannot be tracked
solution: use tracking_id instead of exercise_id....?
Problem: tracking_id and exercise association?

I want to allow using the same exercises in different routines (thats why we have exercises and r_compo tables)
I want to allow separate weight values for same exercises, but used in different routines

To populate the routine table, we need user_id (ion auth); routine_id(selected_routine table)
right now, routine A can have exercise 1 (e.g. squats), and routine B can have exercise 1 as well to avoid duplication
How can we discern A1 from B1? 
Add routine_id to tracking table? or add a new table which associates tracking_id with routine_id?
in the tracking table both user_id and routine_id will have high value duplication... but they will in their own table as well
what would that table look like?
routine_association table: id; tracking id ; routine id
It seems that adding routine_id to the tracking table will be more efficient.... however, wont there be other problems? 
why havent i added routine_id to that table in the first place? I think some joins can be omitted if we do?
Does the routine_composition table become superfluous? No, it seems more efficient to get composition from here than from tracking

----------
tracking id ; user id ; exercise id ; routine id
55		5	11		1
56		5	11		2

How about using routine_composition_id instead of exercise_id? it identifies both routine_id and exercise_id..
 so instead of those two columns in tracking we use one... lets do it!
ofc need to modify model .. and probably will need more joins