-- phpMyAdmin SQL Dump
-- version 4.0.4
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jul 01, 2013 at 12:02 PM
-- Server version: 5.5.27
-- PHP Version: 5.4.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `yaowt`
--
CREATE DATABASE IF NOT EXISTS `yaowt` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `yaowt`;

-- --------------------------------------------------------

--
-- Table structure for table `groups`
--

CREATE TABLE IF NOT EXISTS `groups` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `description` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

-- --------------------------------------------------------

--
-- Table structure for table `login_attempts`
--

CREATE TABLE IF NOT EXISTS `login_attempts` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `ip_address` varbinary(16) NOT NULL,
  `login` varchar(100) NOT NULL,
  `time` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `ip_address` varbinary(16) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(80) NOT NULL,
  `salt` varchar(40) DEFAULT NULL,
  `email` varchar(100) NOT NULL,
  `activation_code` varchar(40) DEFAULT NULL,
  `forgotten_password_code` varchar(40) DEFAULT NULL,
  `forgotten_password_time` int(11) unsigned DEFAULT NULL,
  `remember_code` varchar(40) DEFAULT NULL,
  `created_on` int(11) unsigned NOT NULL,
  `last_login` int(11) unsigned DEFAULT NULL,
  `active` tinyint(1) unsigned DEFAULT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `company` varchar(100) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;

-- --------------------------------------------------------

--
-- Table structure for table `users_groups`
--

CREATE TABLE IF NOT EXISTS `users_groups` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned NOT NULL,
  `group_id` mediumint(8) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uc_users_groups` (`user_id`,`group_id`),
  KEY `fk_users_groups_users1_idx` (`user_id`),
  KEY `fk_users_groups_groups1_idx` (`group_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=26 ;

--
-- RELATIONS FOR TABLE `users_groups`:
--   `group_id`
--       `groups` -> `id`
--   `user_id`
--       `users` -> `id`
--

-- --------------------------------------------------------

--
-- Table structure for table `yaowt_conditions`
--

CREATE TABLE IF NOT EXISTS `yaowt_conditions` (
  `condition_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `event_id` int(11) unsigned NOT NULL,
  `condition_name` varchar(512) DEFAULT NULL,
  `condition_description` varchar(512) DEFAULT NULL,
  `condition_logic` text,
  PRIMARY KEY (`condition_id`),
  KEY `event_id` (`event_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `yaowt_equipment`
--

CREATE TABLE IF NOT EXISTS `yaowt_equipment` (
  `yaowt_equipment_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `equipment_description` varchar(512) NOT NULL,
  PRIMARY KEY (`yaowt_equipment_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `yaowt_events`
--

CREATE TABLE IF NOT EXISTS `yaowt_events` (
  `event_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `event_name` varchar(512) DEFAULT NULL,
  `event_description` varchar(512) DEFAULT NULL,
  PRIMARY KEY (`event_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

-- --------------------------------------------------------

--
-- Table structure for table `yaowt_exercises`
--

CREATE TABLE IF NOT EXISTS `yaowt_exercises` (
  `exercise_id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Information about the exercises.',
  `exercise_name` varchar(255) NOT NULL,
  `exercise_instructions` varchar(512) NOT NULL,
  `exercise_media` varchar(512) NOT NULL,
  PRIMARY KEY (`exercise_id`),
  UNIQUE KEY `exercise_name` (`exercise_name`,`exercise_instructions`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=22 ;

-- --------------------------------------------------------

--
-- Table structure for table `yaowt_routines`
--

CREATE TABLE IF NOT EXISTS `yaowt_routines` (
  `routine_id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Information about the routines.',
  `routine_name` varchar(45) DEFAULT NULL,
  `routine_info` varchar(512) DEFAULT NULL,
  `routine_media` varchar(512) DEFAULT NULL,
  PRIMARY KEY (`routine_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

-- --------------------------------------------------------

--
-- Table structure for table `yaowt_routine_composition`
--

CREATE TABLE IF NOT EXISTS `yaowt_routine_composition` (
  `routine_composition_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `routine_id` int(11) unsigned NOT NULL,
  `exercise_id` int(11) unsigned NOT NULL,
  PRIMARY KEY (`routine_composition_id`),
  KEY `routine_id` (`routine_id`),
  KEY `exercise_id` (`exercise_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;

--
-- RELATIONS FOR TABLE `yaowt_routine_composition`:
--   `routine_id`
--       `yaowt_routines` -> `routine_id`
--   `exercise_id`
--       `yaowt_exercises` -> `exercise_id`
--

-- --------------------------------------------------------

--
-- Table structure for table `yaowt_routine_tracking`
--

CREATE TABLE IF NOT EXISTS `yaowt_routine_tracking` (
  `routine_tracking_id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Tracks which routine(s)[0~N] a user is doing.',
  `user_id` int(11) unsigned NOT NULL,
  `routine_id` int(11) unsigned NOT NULL,
  `starting_date` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`routine_tracking_id`),
  UNIQUE KEY `user_id` (`user_id`,`routine_id`),
  KEY `fk_yaowt_routine_tracking_users1_idx` (`user_id`),
  KEY `fk_yaowt_routine_tracking_yaowt_routines1_idx` (`routine_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=21 ;

--
-- RELATIONS FOR TABLE `yaowt_routine_tracking`:
--   `routine_id`
--       `yaowt_routines` -> `routine_id`
--   `user_id`
--       `users` -> `id`
--

-- --------------------------------------------------------

--
-- Table structure for table `yaowt_selected_routine`
--

CREATE TABLE IF NOT EXISTS `yaowt_selected_routine` (
  `selected_routine_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned NOT NULL,
  `routine_id` int(11) unsigned DEFAULT NULL,
  `split_routine_id` int(11) unsigned DEFAULT NULL,
  `split_id` int(11) unsigned DEFAULT NULL COMMENT 'DELETE ME',
  PRIMARY KEY (`selected_routine_id`),
  UNIQUE KEY `user_id_2` (`user_id`),
  KEY `user_id` (`user_id`),
  KEY `routine_id` (`routine_id`),
  KEY `split_routine_id` (`split_routine_id`),
  KEY `split_id` (`split_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=30 ;

--
-- RELATIONS FOR TABLE `yaowt_selected_routine`:
--   `routine_id`
--       `yaowt_routines` -> `routine_id`
--   `split_routine_id`
--       `yaowt_split_routines` -> `split_routine_id`
--   `user_id`
--       `users` -> `id`
--   `split_id`
--       `yaowt_splits` -> `split_id`
--

-- --------------------------------------------------------

--
-- Table structure for table `yaowt_splits`
--

CREATE TABLE IF NOT EXISTS `yaowt_splits` (
  `split_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `split_number` tinyint(4) unsigned NOT NULL,
  `split_name` varchar(255) DEFAULT NULL,
  `split_info` varchar(2000) DEFAULT NULL,
  PRIMARY KEY (`split_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=44 ;

-- --------------------------------------------------------

--
-- Table structure for table `yaowt_split_composition`
--

CREATE TABLE IF NOT EXISTS `yaowt_split_composition` (
  `split_composition_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `split_id` int(11) unsigned NOT NULL,
  `exercise_id` int(11) unsigned NOT NULL,
  PRIMARY KEY (`split_composition_id`),
  KEY `split_id` (`split_id`),
  KEY `exercise_id` (`exercise_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=51 ;

--
-- RELATIONS FOR TABLE `yaowt_split_composition`:
--   `split_id`
--       `yaowt_splits` -> `split_id`
--   `exercise_id`
--       `yaowt_exercises` -> `exercise_id`
--

-- --------------------------------------------------------

--
-- Table structure for table `yaowt_split_exercise_details`
--

CREATE TABLE IF NOT EXISTS `yaowt_split_exercise_details` (
  `split_exercise_details_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `split_composition_id` int(11) unsigned NOT NULL,
  `sets` int(11) unsigned DEFAULT NULL,
  `reps` int(11) unsigned DEFAULT NULL,
  `notes` varchar(512) DEFAULT NULL,
  PRIMARY KEY (`split_exercise_details_id`),
  KEY `split_composition_id` (`split_composition_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=38 ;

--
-- RELATIONS FOR TABLE `yaowt_split_exercise_details`:
--   `split_composition_id`
--       `yaowt_split_composition` -> `split_composition_id`
--

-- --------------------------------------------------------

--
-- Table structure for table `yaowt_split_routines`
--

CREATE TABLE IF NOT EXISTS `yaowt_split_routines` (
  `split_routine_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `routine_name` varchar(45) DEFAULT NULL,
  `routine_info` varchar(512) DEFAULT NULL,
  `required_equipment_id` int(11) unsigned NOT NULL,
  `routine_media` varchar(512) DEFAULT NULL,
  `success_condition_id` int(11) unsigned DEFAULT NULL,
  `success_event_id` int(11) unsigned DEFAULT NULL,
  `fail_event_id` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`split_routine_id`),
  KEY `success_event_id` (`success_event_id`),
  KEY `fail_event_id` (`fail_event_id`),
  KEY `required_equipment_id` (`required_equipment_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=34 ;

--
-- RELATIONS FOR TABLE `yaowt_split_routines`:
--   `success_event_id`
--       `yaowt_events` -> `event_id`
--   `fail_event_id`
--       `yaowt_events` -> `event_id`
--

-- --------------------------------------------------------

--
-- Table structure for table `yaowt_split_routine_composition`
--

CREATE TABLE IF NOT EXISTS `yaowt_split_routine_composition` (
  `split_routine_composition_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `split_routine_id` int(11) unsigned NOT NULL,
  `split_id` int(11) unsigned NOT NULL,
  PRIMARY KEY (`split_routine_composition_id`),
  KEY `split_id` (`split_id`),
  KEY `split_routine_id` (`split_routine_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=27 ;

--
-- RELATIONS FOR TABLE `yaowt_split_routine_composition`:
--   `split_id`
--       `yaowt_splits` -> `split_id`
--   `split_routine_id`
--       `yaowt_split_routines` -> `split_routine_id`
--

-- --------------------------------------------------------

--
-- Table structure for table `yaowt_split_routine_tracking`
--

CREATE TABLE IF NOT EXISTS `yaowt_split_routine_tracking` (
  `split_tracking_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned NOT NULL,
  `split_routine_id` int(11) unsigned NOT NULL,
  `starting_date` timestamp NULL DEFAULT NULL,
  `view_split_manually` tinyint(1) NOT NULL DEFAULT '0',
  `selected_split_id` int(11) unsigned DEFAULT NULL,
  `condition_id` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`split_tracking_id`),
  UNIQUE KEY `user_id` (`user_id`,`split_routine_id`),
  KEY `split_routine_id` (`split_routine_id`),
  KEY `user_id_2` (`user_id`),
  KEY `selected_split_id` (`selected_split_id`),
  KEY `condition_id` (`condition_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=43 ;

--
-- RELATIONS FOR TABLE `yaowt_split_routine_tracking`:
--   `user_id`
--       `users` -> `id`
--   `split_routine_id`
--       `yaowt_split_routines` -> `split_routine_id`
--   `selected_split_id`
--       `yaowt_splits` -> `split_id`
--   `condition_id`
--       `yaowt_events` -> `event_id`
--

-- --------------------------------------------------------

--
-- Table structure for table `yaowt_tracking`
--

CREATE TABLE IF NOT EXISTS `yaowt_tracking` (
  `tracking_id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Tracks which exercises a user is doing and user-related information about those exercises.',
  `user_id` int(11) unsigned NOT NULL,
  `routine_composition_id` int(11) unsigned DEFAULT NULL,
  `split_composition_id` int(11) unsigned DEFAULT NULL,
  `start_date` timestamp NULL DEFAULT NULL,
  `start_weight` decimal(6,2) unsigned DEFAULT NULL,
  `current_weight` decimal(6,2) unsigned DEFAULT NULL,
  `weight_as_percentage` tinyint(1) NOT NULL DEFAULT '0',
  `reps` int(11) unsigned DEFAULT NULL,
  `sets` int(11) unsigned DEFAULT NULL,
  `repeat_cycle` tinyint(1) NOT NULL DEFAULT '0',
  `user_notes` varchar(512) DEFAULT NULL,
  PRIMARY KEY (`tracking_id`),
  UNIQUE KEY `user_id` (`user_id`,`routine_composition_id`),
  UNIQUE KEY `user_id_2` (`user_id`,`split_composition_id`),
  KEY `fk_yaowt_tracking_users1_idx` (`user_id`),
  KEY `routine_composition_id` (`routine_composition_id`),
  KEY `split_composition_id` (`split_composition_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=227 ;

--
-- RELATIONS FOR TABLE `yaowt_tracking`:
--   `user_id`
--       `users` -> `id`
--   `routine_composition_id`
--       `yaowt_routine_composition` -> `routine_composition_id`
--   `split_composition_id`
--       `yaowt_split_composition` -> `split_composition_id`
--

-- --------------------------------------------------------

--
-- Table structure for table `yaowt_tracking_history`
--

CREATE TABLE IF NOT EXISTS `yaowt_tracking_history` (
  `tracking_history_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `tracking_id` int(11) unsigned NOT NULL,
  `tracking_timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `weight` decimal(6,2) unsigned NOT NULL DEFAULT '0.00',
  `reps` int(11) unsigned DEFAULT NULL,
  `sets` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`tracking_history_id`),
  KEY `tracking_id` (`tracking_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `users_groups`
--
ALTER TABLE `users_groups`
  ADD CONSTRAINT `fk_users_groups_groups1` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `users_groups_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `yaowt_routine_composition`
--
ALTER TABLE `yaowt_routine_composition`
  ADD CONSTRAINT `yaowt_routine_composition_ibfk_1` FOREIGN KEY (`routine_id`) REFERENCES `yaowt_routines` (`routine_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `yaowt_routine_composition_ibfk_2` FOREIGN KEY (`exercise_id`) REFERENCES `yaowt_exercises` (`exercise_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `yaowt_routine_tracking`
--
ALTER TABLE `yaowt_routine_tracking`
  ADD CONSTRAINT `fk_yaowt_routine_tracking_yaowt_routines1` FOREIGN KEY (`routine_id`) REFERENCES `yaowt_routines` (`routine_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `yaowt_routine_tracking_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `yaowt_selected_routine`
--
ALTER TABLE `yaowt_selected_routine`
  ADD CONSTRAINT `yaowt_selected_routine_ibfk_2` FOREIGN KEY (`routine_id`) REFERENCES `yaowt_routines` (`routine_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `yaowt_selected_routine_ibfk_3` FOREIGN KEY (`split_routine_id`) REFERENCES `yaowt_split_routines` (`split_routine_id`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `yaowt_selected_routine_ibfk_4` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `yaowt_selected_routine_ibfk_5` FOREIGN KEY (`split_id`) REFERENCES `yaowt_splits` (`split_id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Constraints for table `yaowt_split_composition`
--
ALTER TABLE `yaowt_split_composition`
  ADD CONSTRAINT `yaowt_split_composition_ibfk_1` FOREIGN KEY (`split_id`) REFERENCES `yaowt_splits` (`split_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `yaowt_split_composition_ibfk_2` FOREIGN KEY (`exercise_id`) REFERENCES `yaowt_exercises` (`exercise_id`) ON DELETE NO ACTION ON UPDATE CASCADE;

--
-- Constraints for table `yaowt_split_exercise_details`
--
ALTER TABLE `yaowt_split_exercise_details`
  ADD CONSTRAINT `yaowt_split_exercise_details_ibfk_2` FOREIGN KEY (`split_composition_id`) REFERENCES `yaowt_split_composition` (`split_composition_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `yaowt_split_routines`
--
ALTER TABLE `yaowt_split_routines`
  ADD CONSTRAINT `yaowt_split_routines_ibfk_1` FOREIGN KEY (`success_event_id`) REFERENCES `yaowt_events` (`event_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `yaowt_split_routines_ibfk_2` FOREIGN KEY (`fail_event_id`) REFERENCES `yaowt_events` (`event_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `yaowt_split_routine_composition`
--
ALTER TABLE `yaowt_split_routine_composition`
  ADD CONSTRAINT `yaowt_split_routine_composition_ibfk_2` FOREIGN KEY (`split_id`) REFERENCES `yaowt_splits` (`split_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `yaowt_split_routine_composition_ibfk_3` FOREIGN KEY (`split_routine_id`) REFERENCES `yaowt_split_routines` (`split_routine_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `yaowt_split_routine_tracking`
--
ALTER TABLE `yaowt_split_routine_tracking`
  ADD CONSTRAINT `yaowt_split_routine_tracking_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  ADD CONSTRAINT `yaowt_split_routine_tracking_ibfk_3` FOREIGN KEY (`split_routine_id`) REFERENCES `yaowt_split_routines` (`split_routine_id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  ADD CONSTRAINT `yaowt_split_routine_tracking_ibfk_4` FOREIGN KEY (`selected_split_id`) REFERENCES `yaowt_splits` (`split_id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  ADD CONSTRAINT `yaowt_split_routine_tracking_ibfk_5` FOREIGN KEY (`condition_id`) REFERENCES `yaowt_events` (`event_id`) ON DELETE NO ACTION ON UPDATE CASCADE;

--
-- Constraints for table `yaowt_tracking`
--
ALTER TABLE `yaowt_tracking`
  ADD CONSTRAINT `yaowt_tracking_ibfk_5` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  ADD CONSTRAINT `yaowt_tracking_ibfk_6` FOREIGN KEY (`routine_composition_id`) REFERENCES `yaowt_routine_composition` (`routine_composition_id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  ADD CONSTRAINT `yaowt_tracking_ibfk_7` FOREIGN KEY (`split_composition_id`) REFERENCES `yaowt_split_composition` (`split_composition_id`) ON DELETE SET NULL ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
