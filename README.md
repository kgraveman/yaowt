# YAOWT

YAOWT is my first 'no training wheels' web application project. It is a workout
tracker. You can create split routines and then schedule workouts by setting
a starting date and filling out starting weight values. Currently the 'Simple
Beginner's Routine' by All Pro supports automatic rep value increases and 
weight increases as per the routine's instructions. The framework is present to
extend this functionality (condition based events) to other routines.
Other features such as AJAX searching and jQuery graphs are also in the 
pipeline.
... I still don't get how git works...
So first I commited without staging files, on push nothing was pushed... then
I made some changes, tried pushing again, but that failed because pointer was
behind. So I pulled. Now I'll change this, stage, and push, and see what happens.


## On SD prefixes

Files prefixed with SD are files that may contain sensitive data. These files
should never be committed when there is sensitive data present.
Currently these files are:
config.php, database.php and ion_auth.php
they all (3 files) reside in /application/config.
I also added the real (non-prefixed) variants to the .gitignore repo file.
Not sure if that is working as it should though.
For (my) future reference: the sensitive data is stored in sensitivedata.txt
somewhere in a backup folder.


# [HTML5 Boilerplate](http://html5boilerplate.com)

HTML5 Boilerplate is a professional front-end template for building fast,
robust, and adaptable web apps or sites.

This project is the product of many years of iterative development and combined
community knowledge. It does not impose a specific development philosophy or
framework, so you're free to architect your code in the way that you want.

* Source: [https://github.com/h5bp/html5-boilerplate](https://github.com/h5bp/html5-boilerplate)
* Homepage: [http://html5boilerplate.com](http://html5boilerplate.com)
* Twitter: [@h5bp](http://twitter.com/h5bp)


## Quick start

Choose one of the following options:

1. Download the latest stable release from
   [html5boilerplate.com](http://html5boilerplate.com/) or a custom build from
   [Initializr](http://www.initializr.com).
2. Clone the git repo — `git clone
   https://github.com/h5bp/html5-boilerplate.git` - and checkout the tagged
   release you'd like to use.


## Features

* HTML5 ready. Use the new elements with confidence.
* Cross-browser compatible (Chrome, Opera, Safari, Firefox 3.6+, IE6+).
* Designed with progressive enhancement in mind.
* Includes [Normalize.css](http://necolas.github.com/normalize.css/) for CSS
  normalizations and common bug fixes.
* The latest [jQuery](http://jquery.com/) via CDN, with a local fallback.
* The latest [Modernizr](http://modernizr.com/) build for feature detection.
* IE-specific classes for easier cross-browser control.
* Placeholder CSS Media Queries.
* Useful CSS helpers.
* Default print CSS, performance optimized.
* Protection against any stray `console.log` causing JavaScript errors in
  IE6/7.
* An optimized Google Analytics snippet.
* Apache server caching, compression, and other configuration defaults for
  Grade-A performance.
* Cross-domain Ajax and Flash.
* "Delete-key friendly." Easy to strip out parts you don't need.
* Extensive inline and accompanying documentation.


## Documentation

Take a look at the [documentation table of contents](doc/TOC.md). This
documentation is bundled with the project, which makes it readily available for
offline reading and provides a useful starting point for any documentation you
want to write about your project.


## Contributing

Anyone and everyone is welcome to [contribute](CONTRIBUTING.md). Hundreds of
developers have helped make the HTML5 Boilerplate what it is today.
