<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

//common controller helpers

function check_logged_in()
	{
		$ci =& get_instance();
		if ($ci->ion_auth->user()->row()) {		
			$user_id = $ci->ion_auth->user()->row()->id;
			return $user_id;
		} else {
			//echo 'not logged in!';
			redirect(base_url()); //TODO send flashdata
		}
	}

//routine_model helpers

function routine_type($type)
{
	if ($type == 'split') {
		return 'split_';
	} else {
		return '';
	}
	
}

function split_or_routine($type)
{
	if ($type == 'split') {
		return 'split';
	}elseif ($type == 'routine') {
		return 'routine';
	} 
	else {
		echo 'Design error:helper:split_or_routine, please contact admin.';
		exit;
	}
	
}

function _required($required, $data)
{
	foreach ($required as $field) {
		if (!isset($data[$field])) {
			return FALSE;
		}
	}
	return TRUE;
}

function _default($defaults, $options)
{
	return array_merge($defaults, $options);
}

//source: http://stackoverflow.com/questions/1168175/is-there-a-pretty-print-for-php
function pp($arr){
    $retStr = '<ul>';
    if (is_array($arr)){
        foreach ($arr as $key=>$val){
            if (is_array($val)){
                $retStr .= '<li>' . $key . ' => ' . pp($val) . '</li>';
            }else{
                $retStr .= '<li>' . $key . ' => ' . $val . '</li>';
            }
        }
    }
    $retStr .= '</ul>';
    return $retStr;
}

function check_ajax()
{
	$ci =& get_instance();
	if ($ci->input->is_ajax_request()) {
		return TRUE;
	}else {
		show_404();
	}
}

function check_post()
{
	$ci =& get_instance();
	if (!$ci->input->post()) {
			show_404();
	}
}

/* End of file yaowt_helper.php */
/* Location: .application/controllers/yaowt_helper.php */