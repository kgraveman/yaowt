<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Builder extends CI_Controller {
	function __construct() {
		parent::__construct();
		$this->load->model('routine_model');
	}
	// public function index()
	// {
		// if ($this->form_validation->run() == FALSE){
// 		
			// check_logged_in();
			// $exercises = $this->routine_model->get_all_exercises();
			// $ex_names = array();
			// foreach ($exercises as $exercise) {
				// array_push($ex_names, $exercise['exercise_name'] . ' [' . 
				// $exercise['exercise_id'] . ']');
			// }
// 			
			// $ids = array(); //XXX ugly
			// foreach ($exercises as $exercise) {
				// array_push($ids, $exercise['exercise_id']);
			// }
// 			
// 			
			// $data = array(
				// 'title' => 'YAOWT Builder',
				// 'content' => 'builder',
				// 'scripts' => 'scripts/builder_script',
				// 'ex_names' => $ex_names,
				// 'ids' => $ids 
			// );
// 			
			// // $this->form_validation->set_rules('r_name', 'routine name', 'required|trim|min_length[5]|max_length[51]|is_unique[yaowt_split_routines.routine_name]');
			// // $this->form_validation->set_rules('r_info', 'routine info', 'trim');
			// // $this->form_validation->set_rules('d_name[]', 'day name', 'trim');
			// // $this->form_validation->set_rules('d_info[]', 'day info', 'trim');
			// // $this->form_validation->set_rules('reps_1[]', 'reps', '');
			// // $this->form_validation->set_rules('sets_1[]', 'sets', '');
			// // $this->form_validation->set_rules('notes_1[]', 'notes', '');
// 	
			// $this->load->view('template', $data);
		// }
	// }
	
	private function get_id($value)
	{
		preg_match('/\[([^]]+)\]/', $value, $match);	
		return $match[1];
	}
	
	public function index() //this used to be submit, then i added validation and it all came back to the CI example by itself... sweet
	{
		check_logged_in();
		
		$this->form_validation->set_rules('r_name', 'routine name', 'required|trim|min_length[5]|max_length[51]|is_unique[yaowt_split_routines.routine_name]');
		$this->form_validation->set_rules('r_info', 'routine info', 'trim');
		$this->form_validation->set_rules('d_name[]', 'day name', 'trim');
		$this->form_validation->set_rules('d_info[]', 'day info', 'trim');
		$this->form_validation->set_rules('reps_1[]', 'reps', '');
		$this->form_validation->set_rules('sets_1[]', 'sets', '');
		$this->form_validation->set_rules('notes_1[]', 'notes', '');
		
		//repopulate if validation fails
		if ($this->form_validation->run() == FALSE) {
			
			$exercises = $this->routine_model->get_all_exercises();
			$ex_names = array();
			foreach ($exercises as $exercise) {
				array_push($ex_names, $exercise['exercise_name'] . ' [' . 
				$exercise['exercise_id'] . ']');
			}
			
			$ids = array(); //XXX ugly
			foreach ($exercises as $exercise) {
				array_push($ids, $exercise['exercise_id']);
			}
			
			$data = array(
				'title' => 'YAOWT Builder',
				'content' => 'builder',
				'scripts' => 'scripts/builder_script',
				'ex_names' => $ex_names,
				'ids' => $ids 
			);
			$this->load->view('template', $data);
			
		}else {

			$routine_name = $this->input->post('r_name');
			$routine_info = $this->input->post('r_info');
			///TODO FUTURE
			//$routine_media = $this->input->post('r_media');  
			//SUCCESS CONDITION
			//SUCCESS EVENT
			//FAIL EVENT
			// end TODO FUTURE
			$split_routine_id = $this->routine_model->insert_split_routine($routine_name, $routine_info);
	
			$day_name = $this->input->post('d_name');
			foreach ($day_name as $key => $value) {
				$split_number = $key + 1;
				list($split_name) = array_slice($this->input->post('d_name'), $key, 1);
				list($split_info) = array_slice($this->input->post('d_info'), $key, 1); 
				$split_id = $this->routine_model->insert_splits($split_number, $split_name, $split_info);
	
				$exercises = $this->input->post('exer_sel_' . $split_number);
				
				foreach ($exercises as $key => $value) {
					
					// $exercise_id = $this->get_id($value); //alternative: pass id to option's value etc. done.
					$exercise_id = $value;
					$this->routine_model->insert_split_composition($split_id, $exercise_id);
		
					$sets_array = $this->input->post('sets_' . $split_number);
					$sets = $sets_array[$key];
					$reps_array = $this->input->post('reps_' . $split_number);
					$reps = $reps_array[$key];
					$notes_array = $this->input->post('notes_' . $split_number);
					$notes = $notes_array[$key];
					
					
				}
				//INSERT split_exercise_details (per day)
				$split_compo_ids = $this->routine_model->get_split_composition_ids($split_id); //UNSURE is split_id here correct?
				foreach ($split_compo_ids as $key => $value) {
					$split_composition_id = $value['split_composition_id'];
					$affected_rows = $this->routine_model->insert_split_exercise_details($split_composition_id, $sets, $reps, $notes);
				}
				
				
				$this->routine_model->insert_split_routine_composition($split_routine_id, $split_id);
			}
			if ($affected_rows >= 1) {
				$this->session->set_flashdata('message', 'Your new routine has been added.'); //confirm success/failure
			} else {
				$this->session->set_flashdata('message', 'Sorry, failed submitting your routine.');
			}
			// $this->session->set_flashdata('message', 'Query sent... TODO: confirm success/failure');
			redirect(base_url('builder'));
		}
	}
		
	// WIP backup for users without javascript?
	private function instructions()
	{
		// print_r($request = ($this->input->post('exer_sel_1')));
		$exercise_id = ($this->input->post('exer_sel_1')); //TODO change 1 here to some post value
		// $exercise_id = $this->get_id($request[0]);
		//get instructions for given exercise_id
		$instructions = $this->routine_model->get_exercise_instructions(array('exercise_id' => $exercise_id[0]));
		echo "Please enable JavaScript to make full use of this website.";
		echo '<br />Instructions for the exercise: ';
		echo $instructions;		
	}

}



/* End of file builder.php */
/* Location: .application/controllers/builder.php */