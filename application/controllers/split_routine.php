<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Split_routine extends CI_Controller {
	function __construct() {
		parent::__construct();
		$this->load->model('routine_model');
		$this->load->model('events_model');
		$this->load->helper('date');
	}
	public function index()
	{
		$user_id = check_logged_in();

		$data = array(
			'title' => 'YAOWT Split routine',
			'content' => 'split_routine'
		);
		$data['messages'] = array();
		//if subbed to at least one routine:
		if ($this->routine_model->count_split_routine_subs($user_id) >= 1) { //XXX else condition?
			//check and set if nothing is set
			$this->routine_model->check_selected_split_routine($user_id);
		}
		
		$selected_split_routine = $this->routine_model->get_selected_routine(array('user_id' => $user_id, 'type' => 'split'));
		//check if $selected_split_routine is set, otherwise  get_split_routine_start_date will throw errors
		if (empty($selected_split_routine)) {
			$this->session->set_flashdata('message', 'Please subscribe to and/or select a split routine first.');
			redirect(base_url('profile'));
		}
		
		$data['start_date'] = $this->routine_model->get_split_routine_start_date($user_id, $selected_split_routine);
		$data['total_splits'] = $this->routine_model->count_splits_in_routine(array('split_routine_id' => $selected_split_routine));
		
		//check if selected_split is set, if not, set it to the first split in the selected routine
		if (!$this->routine_model->get_selected_split(array('user_id' => $user_id, 'split_routine_id' => $selected_split_routine))) {
			$new_selected_split = $this->routine_model->get_split_by_number(array('split_number' => 1, 'selected_split_routine' => $selected_split_routine));
			$this->routine_model->set_selected_split(array('user_id' => $user_id, 'new_split_id' => $new_selected_split, 'split_routine_id' => $selected_split_routine));	
		}
		
		//get 'final split' (part of show success/fail buttons: part of condition/event parser)
		$final_split = $this->routine_model->get_final_split(array('selected_split_routine' => $selected_split_routine));
		$first_split = $this->routine_model->get_first_split(array('selected_split_routine' => $selected_split_routine));
		
		if ($this->routine_model->is_manual_split_view(array('user_id' => $user_id, 'split_routine_id' => $selected_split_routine))) {
			$data['manual_view'] = TRUE;
			$selected_split = $this->routine_model->get_selected_split(array('user_id' => $user_id, 'split_routine_id' => $selected_split_routine));
			$data['condition_buttons'] = $this->check_button_split(array('first_split' => $first_split, 'final_split' => $final_split, 'shown_split' => $selected_split, 'user_id' => $user_id));
		} else {
			$data['manual_view'] = FALSE;
			$start_unix = human_to_unix($data['start_date']);
			$now = time();
			$delta = $now - $start_unix; 
			$days_passed = floor($delta / 86400);
			$split_number = abs(($days_passed % $data['total_splits'] + 1));
			$selected_split = $this->routine_model->get_split_by_number(array('split_number' => $split_number, 'selected_split_routine' => $selected_split_routine));
			$data['condition_buttons'] = $this->check_button_split(array('first_split' => $first_split, 'final_split' => $final_split, 'shown_split' => $selected_split, 'user_id' => $user_id));
		}		
		//end show success/fail buttons
		
		
		$check_start_message = $this->routine_model->check_split_routine_starting_date($user_id, $selected_split_routine);
		array_push($data['messages'], $check_start_message);
		
		$data['routine_name'] = $this->routine_model->get_split_routine_name($user_id, $selected_split_routine);
		
		//empty $data['routine_name'] was here, but live version does not catch it, its too late?
		if (empty($data['routine_name'])) {
			$this->session->set_flashdata('message', 'Please subscribe to and select a split routine before trying to view one.');
			redirect(base_url('browse'));	
		}
		
		$data['exercises'] = $this->routine_model->get_exercises_split($user_id, $selected_split); //XXX split type?
		
		// get split_compo:  (to identify which exercises are in the split)
		//get the split composition id's that belong to the (selected routine AND) selected split
		
	//check for missing rows in tracking table and fill those
		$routine_compo = $this->routine_model->get_split_routine_composition_ids($selected_split_routine);
		
		$routine_compo_ids = array(); ///XXX ugly, lambda filter function or something?
		foreach ($data['exercises'] as $exercise) {			//XXX rename exercise here and there
			array_push($routine_compo_ids, $exercise['split_composition_id']);
		}
		// does tracking table have all r_c_id's stored in $r_compo_ids?
		//tracking table ids: $data['parser_data']['exercises'][$key]['routine_composition_id']
		$missing_detected = FALSE;
		foreach ($routine_compo as $exercise) {
			if (!in_array($exercise['split_composition_id'], $routine_compo_ids)) {
				$missing_detected = TRUE;
				//add the missing tracking data, based on user_id and routine_compo_id
				$routine_compo_id = $exercise['split_composition_id'];
				
				$this->routine_model->add_missing_tracking_row(array('user_id' => $user_id, 'routine_compo_id' => $routine_compo_id, 'type' => 'split'));
			}
		}
		if ($missing_detected) {
			//reload $data['exercises']
			$data['exercises'] = $this->routine_model->get_exercises_split($user_id, $selected_split); //XXX split type?
		}
	//end check
		
		//resume setting data
		// $data['exercises'] = $this->routine_model->get_exercises_split($user_id, $selected_split); // duplicant
		
		//start event based display of data // unsure about the location of this block as of now
		//XXX CONDITIONS HAVE TO BE SET IN THE DATABASE ACCORDING TO THE FOLLOWING CODE! my apologies for shouting.
		//get condition_id
		$condition_id = $this->events_model->get_condition_id(array('user_id' => $user_id, 'split_routine_id' => $selected_split_routine));
		switch ($condition_id) {
			// (CHANGED)if no condition/event is found, remove buttons.
			case NULL:
				//$data['dynamic_script'] = "$('.event_based').remove();";
				break;
			
			case '1':
				break;
			case '2':
				break;
				
			//increase reps by one:
			case '4':
				$data['exercises'] = $this->adjust_reps(1, $data);
				break;
			
			case '5':
				$data['exercises'] = $this->adjust_reps(2, $data);
				break;
				
			case '6':
				$data['exercises'] = $this->adjust_reps(3, $data);
				break;

			case '7':
				$data['exercises'] = $this->adjust_reps(4, $data);
				break;
							
			default:
				$this->session->set_flashdata('alert', 'Error interpreting condition in split controller. Please report'); //XXX error reporting
				break;
		}
		//end event based display of data
		
		
		$data['split_name'] = $this->routine_model->get_split_name(array('split_id' => $selected_split));
		$data['split_number'] = $this->routine_model->get_split_number(array('split_id' => $selected_split));
		$data['split_info'] = $this->routine_model->get_split_info(array('split_id' => $selected_split));
		$data['scripts'] = 'scripts/split_routine_script';
		
		if ($data['split_number'] == 1) {
			array_push($data['messages'], 'If you want to use workout history tracking, be sure to use the "hit/didn\'t hit" buttons below before editing your exercise data values.'); //TODO never show this message again option (cookie or db)
		}
		
		
		$this->load->view('template', $data);
	}

	private function adjust_reps($amount, $data)
	{
		foreach ($data['exercises'] as &$exercise) {
			$exercise['reps'] += $amount;
		}
		unset($exercise);
		return $data['exercises'];
	}

	public function start_routine_now()
	{
		$user_id = check_logged_in();
		if ($this->session->flashdata('allow_start_r_now') != 'allow') { //UNSURE if this is a good way to 'hide' this method
			show_404();
		}
		
		$selected_split_routine = $this->routine_model->get_selected_routine(array('user_id' => $user_id, 'type' => 'split'));
		$this->routine_model->set_routine_start_now(array('user_id' => $user_id, 'selected_routine' => $selected_split_routine, 'type' => 'split'));
		redirect(base_url('split_routine'));
	}
	
	public function update_startdate()
	{
		check_post();
		$this->form_validation->set_rules('datetime', 'start date for this routine', 'callback_datetime_check');
		if ($this->form_validation->run() == TRUE) { //TRUE else reload?
			$start_date = $this->input->post('datetime');
			//FIXME IMPORTANT! validate $start_date (inser_string escapes the query so it should be ok right? yes: http://stackoverflow.com/questions/2401706/
			$user_id = check_logged_in(); //TODO disable update button when not logged in (note that you cant get to it due to redirect anyway)
			//TODO check if submitted start date differs from the original start date (use a hidden field, seems much cheaper than issuing another query)
			$selected_split_routine = $this->routine_model->get_selected_routine(array('user_id' => $user_id, 'type' => 'split'));
			//XXX move to model
			//INSERT INTO yaowt_routine_tracking user_id, routine_id, starting_date VALUES ($user_id, $routine_id, $start_date) 
			//	ON DUPLICATE KEY UPDATE starting_date = $start_date (is the last part ok?)
			$data = array(
				'user_id' => $user_id, 
				'split_routine_id' => $selected_split_routine, 
				'starting_date' => $start_date
			);
			$sql = $this->db->insert_string('yaowt_split_routine_tracking', $data) . ' ON DUPLICATE KEY UPDATE starting_date = "'. $start_date . '"';
			//routine_tracking_id = routine_tracking_id + 2'
			$this->db->query($sql);
			$this->session->set_flashdata('message', 'Start date updated.');
			redirect(base_url('split_routine'));
		} else {
			$this->session->set_flashdata('alert', 'Something went wrong. Are you using the date and time selector interface?'); //XXX TODO error reporting
			redirect(base_url('split_routine'));
		}
		
		
	}
	
	public function datetime_check($str = '')
	{
		check_post();
		if (preg_match('/^\d{4}\-\d{2}\-\d{2} \d{2}:\d{2}:\d{2}$/', $str)) {
			return TRUE;
		} else {
			return FALSE;
		}
	}
	
	public function update_weight()
	{
		check_post();
		//get all fieldnames: get from POST
		$posts = $this->input->post();
		//only need keys, not values
		foreach ($posts as $key => $value) {  //UNSURE these are else ifs because it iterates over ALL posts; one post can only be one field 
			if (substr_count($key, '_SW') > 0) {
				$this->form_validation->set_rules($key, 'starting weight', 'numeric');
			}
			else if (substr_count($key, '_CW') > 0) {
				$this->form_validation->set_rules($key, 'current weight', 'numeric');
			}
			else if (substr_count($key, '_UN') > 0) {
				$this->form_validation->set_rules($key, 'your notes', 'trim');
			}
			else if (substr_count($key, '_R') > 0) {
				$this->form_validation->set_rules($key, 'reps', 'numeric');
			}
			else if (substr_count($key, '_S') > 0) {
				$this->form_validation->set_rules($key, 'sets', 'numeric');
			}
		}
		unset($key);
		unset($value);
		
		if ($this->form_validation->run() === TRUE) {
			//require: selected->split_routine_id->$tracking_id, user_id,
			$user_id = check_logged_in();
			$selected_split_routine = $this->routine_model->get_selected_routine(array('user_id' => $user_id, 'type' => 'split'));
			$selected_split = $this->routine_model->get_selected_split(array('user_id' => $user_id, 'split_routine_id' => $selected_split_routine));
			$data['exercises'] = $this->routine_model->get_exercises_split($user_id, $selected_split);
	
			//for each row
			foreach ($data['exercises'] as $key => $value) {
				$tracking_id = $this->input->post($key); //is this the best way to get this data? clever?
				$new_start_weight = $this->input->post($tracking_id . '_SW');
				$new_current_weight = $this->input->post($tracking_id . '_CW');
				$user_notes = $this->input->post($tracking_id . '_UN');
				$new_user_reps = $this->input->post($tracking_id . '_R');
				$new_user_sets = $this->input->post($tracking_id . '_S');
				
				//retain user_notes if a new note was not added
				// need to do this because notes are shown as placeholder,
				// whereas weight values use the elements value attribute
				// so weight values are 'self-retaining'
				// UNSURE whether to display all values as placeholder instead
				// probably shouldnt because that would complicate the
				// quick weight update feature
				if (!$user_notes) {
					if ($data['exercises'][$key]['user_notes']) {
						$user_notes = $data['exercises'][$key]['user_notes'];
					} else {
						$user_notes = NULL;
					}
				}
				// superfluous validation
				if ((is_numeric($new_start_weight) || ($new_start_weight == NULL)) && (is_numeric($new_current_weight) || ($new_current_weight == NULL))) {
					$this->routine_model->update_track_row($user_id, $tracking_id, $new_start_weight, $new_current_weight, $user_notes, $new_user_reps, $new_user_sets);
				}
			}
			$this->session->set_flashdata('message', 'Values updated!');
			redirect(base_url('split_routine'));
		} else {
			$this->session->set_flashdata('alert', 'Something went wrong. Please use only numeric values in the weight fields. '); //XXX TODO error reporting
			redirect(base_url('split_routine'));
		}
		


	}

	public function goto_split()
	{		
		check_post();
		$user_id = check_logged_in();
		$selected_split_routine = $this->routine_model->get_selected_routine(array('user_id' => $user_id, 'type' => 'split'));
		$selected_split_id = $this->routine_model->get_selected_split(array('user_id' => $user_id, 'split_routine_id' => $selected_split_routine));
		$split_number = $this->routine_model->get_split_number(array('split_id' => $selected_split_id));
		$total_splits = $this->routine_model->count_splits_in_routine(array('split_routine_id' => $selected_split_routine));
		$split_routine_id = $this->routine_model->get_selected_routine(array('user_id' => $user_id, 'type' => 'split'));
		//set yaowt_selected_routine->split_id -1 or if split_id -1 == 0 count_all_results split_routine compo where split_routine_id = selected routine
		//do logic on split number and updating on split id in selected table
		if ($this->input->post('previous')) {
			if (($split_number - 1) <= 0) {
				$new_split_id = $this->routine_model->get_split_by_number(array('split_number' => $total_splits, 'selected_split_routine' => $selected_split_routine));
				$this->routine_model->set_selected_split(array('user_id' => $user_id, 'new_split_id' => $new_split_id, 'split_routine_id' => $selected_split_routine));
			} else {
				$new_split_id = $this->routine_model->get_split_by_number(array('split_number' => ($split_number -1), 'selected_split_routine' => $selected_split_routine));
				$this->routine_model->set_selected_split(array('user_id' => $user_id, 'new_split_id' => $new_split_id, 'split_routine_id' => $selected_split_routine));
			}
			// $split_routine_id = $this->routine_model->get_selected_routine(array('user_id' => $user_id, 'type' => 'split'));
			$this->routine_model->set_manual_split_view(array('user_id' => $user_id, 'bool' => 1, 'split_routine_id' => $split_routine_id));
			redirect(base_url('split_routine'));
		}
		
		else if ($this->input->post('next')) {
			if (($split_number + 1) > $total_splits) { //XXX DRY?
				$new_split_id = $this->routine_model->get_split_by_number(array('split_number' => 1, 'selected_split_routine' => $selected_split_routine));
				$this->routine_model->set_selected_split(array('user_id' => $user_id, 'new_split_id' => $new_split_id, 'split_routine_id' => $selected_split_routine));
			} else {
				$new_split_id = $this->routine_model->get_split_by_number(array('split_number' => ($split_number + 1), 'selected_split_routine' => $selected_split_routine));
				$this->routine_model->set_selected_split(array('user_id' => $user_id, 'new_split_id' => $new_split_id, 'split_routine_id' => $selected_split_routine));
			}
			// $split_routine_id = $this->routine_model->get_selected_routine(array('user_id' => $user_id, 'type' => 'split'));
			$this->routine_model->set_manual_split_view(array('user_id' => $user_id, 'bool' => 1, 'split_routine_id' => $split_routine_id));
			redirect(base_url('split_routine'));
		}

		else if ($this->input->post('planned')) {
			$this->routine_model->set_manual_split_view(array('user_id' => $user_id, 'bool' => 0, 'split_routine_id' => $split_routine_id));
			redirect(base_url('split_routine'));	
		}
		
		else if ($this->input->post('selected')) {
			$this->routine_model->set_manual_split_view(array('user_id' => $user_id, 'bool' => 1, 'split_routine_id' => $split_routine_id));
			redirect(base_url('split_routine'));
		}
		
		
	}
//show contextual buttons if its the first or final day of the routine.
	private function check_button_split($options = array())
	{
		if (_required(array('first_split', 'final_split', 'shown_split', 'user_id'), $options) == FALSE) {
			return FALSE;
		}
		if (($options['final_split'] == $options['shown_split']) || ($options['first_split'] == $options['shown_split'])) {
			// echo 'final day!';
			$selected_split_routine = $this->routine_model->get_selected_routine(array('user_id' => $options['user_id'], 'type' => 'split'));
			$success_id = $this->events_model->get_success_id(array('split_routine_id' => $selected_split_routine)); 
			$fail_id = $this->events_model->get_fail_id(array('split_routine_id' => $selected_split_routine));
			
			return '<button class="event_buttons" id="success" name="success" type="button" value="' . 
				$success_id . '">I hit my workout target' . (($options['first_split'] == $options['shown_split'])?' last time':'') . 
				'</button><button class="event_buttons" id="unrealized" name="unrealized" type="button" value="' . 
				$fail_id . '">I didn\'t hit my workout target' . (($options['first_split'] == $options['shown_split'])?' last time':'') . '</button>';
		} else {
			// echo 'not final day';
			return NULL;
		}
		
	}



}



/* End of file split_routine.php */
/* Location: .application/controllers/split_routine.php */