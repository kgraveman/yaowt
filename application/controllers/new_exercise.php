<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class New_exercise extends CI_Controller {
	function __construct() {
		parent::__construct();
		$this->load->model('routine_model');
		check_logged_in();
	}
	// public function index()
	// {
		// $data = array();
		// $data['title'] = 'YAOWT Add new exercise';
		// $data['content'] = 'new_exercise';
		// $this->load->view('template', $data);
	// }
	
	public function index()
	{
		$this->form_validation->set_rules('name', 'exercise name', 'required|min_length[4]|max_length[50]|trim|is_unique[yaowt_exercises.exercise_name]');
		$this->form_validation->set_rules('instructions', 'exercise instructions', 'required|min_length[5]|max_length[500]|trim');
		
		if ($this->form_validation->run() == FALSE) {
			$data = array();
			$data['title'] = 'YAOWT Add new exercise';
			$data['content'] = 'new_exercise';
			$this->load->view('template', $data);
		} else {
			$name = $this->input->post('name');
			$instructions = $this->input->post('instructions');
			$result = $this->routine_model->add_new_exercise(array('name' => $name, 'instructions' => $instructions));
			if ($result == 0) {
				$this->session->set_flashdata('alert', 'Failed to add your exercise. Please check if an identical exercise already exists.');
			} else if ($result == 1) {
				$this->session->set_flashdata('message', 'Successfully added your new exercise.');
			} else {
				$this->session->set_flashdata('alert', 'Error submitting. Please report this error to the administrator of this site.'); //TODO contact information and error code
			}
			
			redirect(base_url('new_exercise'));
		}
	}
}



/* End of file new_exercise.php */
/* Location: .application/controllers/new_exercise.php */