<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Flot_data extends CI_Controller {
	function __construct() {
		parent::__construct();
		$this->load->model('routine_model');
		$this->load->model('history_model');
		check_ajax();
	}
	
	public function index()
	{
		$action = $this->input->post('action');
		
		// check for example override
		if ($action != 'get_example') {
			if ($this->ion_auth->user()->row()) {
				$user_id = $this->ion_auth->user()->row()->id;
	
				$selected_routine = $this->routine_model->get_selected_routine(array('user_id' => $user_id, 'type' => 'split'));
	
				$exercise_ids = $this->history_model->get_exercise_ids(array('selected_routine' => $selected_routine));
				
				sort($exercise_ids);
	
				$unique_ex_ids = array_unique($exercise_ids, SORT_REGULAR);
				//check if theres history data for the selected routine && user

				$data_num = $this->history_model->check_history_data(array('selected_routine' => $selected_routine, 'user_id' => $user_id));
				
				if ($data_num < 1) {
					//no data, display no data notification
					$action = 'no_data';
				} 
			} else {
				//user is not logged in, send example data
				$action = 'example';
			}
		}
		
		/////////////////// FOR TESTING XXX
		// $action = 'example';
		/////////////////
		
		switch ($action) {
			case 'get_tab_num':
				$this->get_tab_num($unique_ex_ids);
				break;
				
			case 'get_exercise_names':
				$this->get_exercise_names($unique_ex_ids);
				break;
				
			case 'get_example':
			case 'example':
				$example_data = array(
					array('exercise_name' => 'Squats (example)', 'coords' => array(
						array('time' => 1364810315000, 'av_1rm' => 40),
						array('time' => 1367661515000, 'av_1rm' => 45),
						array('time' => 1370080715000, 'av_1rm' => 60),
						array('time' => 1371722315000, 'av_1rm' => 65),
						array('time' => 1372931915000, 'av_1rm' => 75)	
						)
					),
					array('exercise_name' => 'Deadlifts (example)', 'coords' => array(
						array('time' => 1364810315000, 'av_1rm' => 40),
						array('time' => 1367661515000, 'av_1rm' => 60),
						array('time' => 1370080715000, 'av_1rm' => 60),
						array('time' => 1371722315000, 'av_1rm' => 75),
						array('time' => 1372931915000, 'av_1rm' => 80)
						)
					)
				);
				$this->example($example_data);
				break;
				
			case 'no_data':
				echo json_encode('empty');
				break;
				
			case 'get_data':
				$this->get_data_array();
				break;
			
			default:
				
				break;
		}
	}

	private function example($example_data)
	{
			print json_encode($example_data);
	}

	private function get_tab_num($unique_ex_ids)
	{
			echo count($unique_ex_ids);
	}
	
	private function get_exercise_names($unique_ex_ids)
	{
		$exercise_names = array();
		foreach ($unique_ex_ids as $key => $exercise_id) {
			$exercise_name = $this->history_model->get_exercise_name(array('exercise_id' => $exercise_id['exercise_id']));
			array_push($exercise_names, $exercise_name);
		}
		print json_encode($exercise_names);
	}

	private function get_data_array()
	{
		//displaying of flot graphs
		//for each unique exercise_id, for each unique timestamp in user's tracking history for that exercise_id, calculate the average 1rm
		// checking again if user is logged in SUPERFLUOUS XXX
		if ($this->ion_auth->user()->row()) {
			$user_id = $this->ion_auth->user()->row()->id;
			$selected_routine = $this->routine_model->get_selected_routine(array('user_id' => $user_id, 'type' => 'split'));
			$exercise_ids = $this->history_model->get_exercise_ids(array('selected_routine' => $selected_routine));

			//check for history data for the selected routine && user

			//tracking_id -> split_composition_id ->     split_id ->    y_spl_r_compo -> y_selected_routine.split_routine_id
			//where selected AND user(tracking)
			$data_num = $this->history_model->check_history_data(array('selected_routine' => $selected_routine, 'user_id' => $user_id));

			if ($data_num >= 1) {
				//SORT $exercise_ids
				sort($exercise_ids);

				$unique_ex_ids = array_unique($exercise_ids, SORT_REGULAR);
				$average_per_time_per_exercise = array();

				//tab level (unique exercise)
				foreach ($unique_ex_ids as $key => $exercise_id) {
					$exercise_history = $this->history_model->get_exercise_history(array('exercise_id' => $exercise_id['exercise_id'], 'user_id' => $user_id, 'routine_id' => $selected_routine));

					//get the average 1RM for this exercise TODO allow the user to select max/min/average/select/total work
					//using the Epley formula 1RM = w*r/30+w
					//if timestamp is identical (UNSURE if should use margin
					// then average of 1RMs
					// else, 'create' another flot

					foreach ($exercise_history as $key2 => $row){
						$time[$key2] = $row['tracking_timestamp'];
						$weight[$key2] = $row['weight'];
					}
					unset($key2, $row);

					array_multisort($time, SORT_ASC, $weight, SORT_ASC, $exercise_history); //sorting by weight doesnt seem necessary
					unset($time, $weight);
					//then grab all timestamps and store them

					$timestamps = array();
					foreach ($exercise_history as $key2 => $value2) {
						//make it compatible with flot (unix milliseconds)
						$value2['tracking_timestamp'] = human_to_unix($value2['tracking_timestamp']) * 1000;
						if (!in_array($value2['tracking_timestamp'] , $timestamps)) { 
							array_push($timestamps, $value2['tracking_timestamp']);
						}
					}
					unset($key2, $value2);
					
					//for each of the exercises PER timestamp, calculate a 1rm
					$average_1rm_per_time = array(); //push this to array (per exercise)
					foreach ($timestamps as $key2 => $timestamp) {
						$num_exercises = 0;
						$cumulative = 0;
						$average_1rm = 0;
						foreach ($exercise_history as $key2 => $value2) {
							$value2['tracking_timestamp'] = (human_to_unix($value2['tracking_timestamp']) * 1000); //FIXME MAYBE NOT but this probably already is unix now

							//where tracking_timestamp = timestamp
							//look for multiple identical exercise types
							if ($value2['tracking_timestamp'] == $timestamp) {
								$num_exercises ++;
								//here calculate 1rm (for each of this exercise at this timestamp) using Epley formula
								$this_rep_max = ($value2['weight'] * $value2['reps'] / 30 + $value2['weight']);
								// $this_rep_max = round($this_rep_max, 2);
								$cumulative += $this_rep_max;
							}
						}
						unset($key2, $value2);
						
						$average_1rm = round(($cumulative / $num_exercises),2);
						array_push($average_1rm_per_time, (array('time' => $timestamp, 'av_1rm' => $average_1rm)));
					}
					unset($key2, $value2);
					$exercise_name = $this->history_model->get_exercise_name(array('exercise_id' => $exercise_id['exercise_id']));
					array_push($average_per_time_per_exercise, (array('exercise_name' => $exercise_name, 'coords' => $average_1rm_per_time)));					
				}
				unset($key, $exercise_id);

				echo json_encode($average_per_time_per_exercise);
			}
		//nothing to display TODO SUPERFLUOUS times two! already checked before switch
		}
	}
}


/* End of file flot_data.php */
/* Location: .application/controllers/flot_data.php */