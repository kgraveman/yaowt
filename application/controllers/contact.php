<?php
if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Contact extends CI_Controller {
	function __construct() {
		parent::__construct();

	}

	public function index() {
	//UNSURE what the source of this code is not sure if i wrote it myself.
		//setup email library
		$this -> load -> library(array('form_validation', 'session', 'email'));

		$this -> email -> from('admin@graveman.co', 'Graveman');
		$this -> email -> to('admin@graveman.co');
		$this -> email -> subject('Contact from your Homepage form');

		//setup CAPTCHA
		$this -> load -> helper('captcha');
		$vals = array('img_path' => './captcha/', 'img_url' => './captcha/', 'font_path' => './fonts/LinLibertine_RI_G.ttf', 'img_width' => 150, 'img_height' => 30, 'expiration' => 7200);

		//set validation rules
		$this -> form_validation -> set_rules('sender', '<span style="color: red">Name</span>', 'trim|required|max_length[200]|xss_clean');
		$this -> form_validation -> set_rules('email_r', '<span style="color: red">Email</span>', 'trim|required|valid_email|max_length[200]|xss_clean');
		$this -> form_validation -> set_rules('message_body', '\'Body\'', 'trim|required|max_length[10000]|xss_clean');

		//honeypot against spam bots
		$this -> form_validation -> set_rules('comment_h', 'hmmm', 'callback_comment_h_check');
		$this -> form_validation -> set_rules('email', 'Email', 'callback_email_check');

		//check the CAPTCHA
		$this -> form_validation -> set_rules('captcha', 'CAPTCHA', 'callback_captcha_check');

		//validate input. if pass: send to model
		if ($this -> form_validation -> run() == FALSE) {

			$data = array('title' => 'Contact', 'content' => 'contact');

			//add the captcha to the data
			$data['cap'] = create_captcha($vals);

			//process CAPTCHA data - consider moving this to a model
			$captcha_data = array('captcha_time' => $data['cap']['time'], 'ip_address' => $this -> input -> ip_address(), 'word' => $data['cap']['word']);
			$captcha_query = $this -> db -> insert_string('captcha', $captcha_data);
			$this -> db -> query($captcha_query);

			//show the contact page
			// $this -> parser -> parse('template', $data);
			$this->load->view('template', $data);

		} else {

			$message_body = $this -> input -> post('message_body');
			$sender = $this -> input -> post('sender');
			$email = strtolower($this -> input -> post('email_r'));

			$form_body = $sender . '(' . $email . ') has sent you a message: ' . $message_body;

			$this -> email -> message($form_body);

			$this -> email -> send();

			$this -> session -> set_flashdata('message', 'Thank you! Your message has been sent.');

			//redirect to same page so view can display flashdata?
			redirect(base_url());
		}
	}

	public function comment_h_check($str) {
		check_post();
		if ($str == '') {
			return TRUE;
		} else {
			show_404();
			return FALSE;
		}
	}

	public function email_check($str) {
		check_post();
		if ($str == '') {
			return TRUE;
		} else {
			show_404();
			return FALSE;
		}
	}

	public function captcha_check($str) {
		check_post();

		// First, delete old captchas
		$expiration = time() - 7200;
		// Two hour limit
		$this -> db -> query("DELETE FROM captcha WHERE captcha_time < " . $expiration);

		// Then see if a captcha exists:
		$sql = "SELECT COUNT(*) AS count FROM captcha WHERE word = ? AND ip_address = ? AND captcha_time > ?";
		$binds = array($_POST['captcha'], $this -> input -> ip_address(), $expiration);
		$query = $this -> db -> query($sql, $binds);
		$row = $query -> row();

		if ($row -> count == 0) {
			$this->form_validation->set_message('captcha_check', '<span class="error">You must submit the word that appears in the CAPTCHA image.</span>');
			return FALSE;
		} else {
			return TRUE;
		}
	}

}

/* End of file contact.php */
/* Location: .application/controllers/contact.php */
