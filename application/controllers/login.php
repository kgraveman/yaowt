<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Login extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		
		// if logged in, go to overview (logged in)
		// if not logged in, go to overview (not logged in, showing login/signup link) (it seems this file will be migrated to overview?)
		
		// $this->lang->load('auth', 'english'); //not required, auth does this
				
		if (!$this->ion_auth->logged_in()) {	
			
			// $data = array(                   //not required auth does this
				// //'message' => 'DEVELOPMENT',
				// 'title' => 'YAOWT Login',
				// 'content' => 'auth/login',
				// 'identity' => array(
					// 'type' => 'text',
					// 'name' => 'username',
					// 'id' => 'username',
					// 'placeholder' => 'Username',
					// 'maxlength' => '100'
				// ), 
				// 'password' => array(
					// 'type' => 'password',
					// 'name' => 'password',
					// 'id' => 'password',
					// 'placeholder' => 'Password',
					// 'maxlength' => '50'
				// )
			// );
			
			//$this->load->view('template', $data); //this doesnt seem to work, use redirect instead?!
			redirect(base_url() . 'auth/login');
		}
		else {
			header('Location: ' . base_url());
			die();	
		}
		
		// if (1) {
			// $data = array(
				// //'message' => 'DEVELOPMENT',
				// 'identity' => array(
					// 'name' => 'username',
					// 'id' => 'username',
					// 'placeholder' => 'Username',
					// 'maxlength' => '100'
				// ), 
				// 'password' => array(
					// 'name' => 'password',
					// 'id' => 'password',
					// 'placeholder' => 'Password',
					// 'maxlength' => '50'
				// )
			// );
			// $this->load->view('auth/login', $data);
			
			
			//$identity = 'admin@admin.com';
			
			//$password = 'password';
			
			//$remember = FALSE;
			
			//$this->ion_auth->login($identity, $password, $remember);
			
// 			
		// } else {
			// header('Location: http://localhost/yaowt/overview');
			// die();	
		// }
// 		
// 		
		
		
		
		//add conditional here
		// if login fails redirect to overview controller
		
		
		// $this->load->library('parser');
		// $data = array(
		// 'error' => 'error, controller did not break');
		// $this->load->view('template');
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */