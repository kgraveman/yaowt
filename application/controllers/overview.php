<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Overview extends CI_Controller {
	function __construct() {
		parent::__construct();
		$this->load->model('history_model');
		$this->load->model('routine_model');
	}
	public function index()
	{
		$this->load->library('parser');
		$data = array(
			'title' => 'YAOWT Overview',
			'content' => 'overview',
			'scripts' => 'scripts/overview_script'
		);
		 
		$this->load->view('template', $data);
	}
}

/* End of file overview.php */
/* Location: .application/controllers/overview.php */