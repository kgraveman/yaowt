<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ajax extends CI_Controller {
	function __construct() {
		parent::__construct();
		$this->load->model('routine_model');
		$this->load->model('events_model');
		$this->load->model('history_model');
		check_ajax();
	}
	public function index()
	{
		$action = $this->input->post('action');
		
		switch ($action) {
			//TODO check if data is set (all cases!)
			
			// get instructions for an exercise (builder)
			case 'get_instructions':
				$this->get_instructions($this->input->post('data'));
				break;
			//do the event associated with the condition id (view routine)
			// AND record exercise history
			case 'run_condition':
				$this->run_condition($this->input->post('data'));
				break;
			
			default:
				echo 'Error in ajax script. Please report this error to the administrator.'; //XXX TODO error reporting			
				break;
		}
	}
	
	public function get_instructions($exercise_id)
	{
		if ($exercise_id) { //XXX superfluous, function will fail if var not set anyway
			echo $this->routine_model->get_exercise_instructions(array('exercise_id' => $exercise_id));
		}
	}
	
	//do condition logic and set new condition_id
	public function run_condition($condition_id)
	{
		// echo '...run condition start...';
		$user_id = check_logged_in();
		
	//new feature: regardless of which button was pressed, record the users efforts
		$now = time();
		// $allow_logging = FALSE;
		$last_log = $this->routine_model->get_last_log_time(array('user_id' => $user_id));
		if ($last_log == 0) {
			$allow_logging = TRUE;

			$delta_human = 'This was your first workout log for this routine. ';
		} else {
			$last_log = human_to_unix($last_log);
		
			$allow_logging = FALSE;
			$delta = $now - $last_log;
			if ($delta < 3600) {
				$delta_human = ($delta / 60);
				$delta_human = round($delta_human);
				$delta_human = 'Before this, you logged workout data ' . $delta_human . ' minutes ago. ';
			}
			else if ($delta < 86400) {
				//return difference in hours
				$delta_human = ($delta / 3600);
				$delta_human = round($delta_human, 1);
				$delta_human = 'Before this, you logged workout data ' . $delta_human . ' hours ago. ';
			//more than a day
			} else if ($delta > 86400) {
				$allow_logging = TRUE;
				//return difference in days
				$delta_human = $delta / 86400;
				$delta_human = round($delta_human, 1);
				$delta_human = 'Before this, you logged workout data ' . $delta_human . ' days ago. '; 
			}
		}
		
		if ($allow_logging) {
			echo 'Your workout data has been logged. ' . $delta_human; //TODO allow undo
			$selected_split_routine = $this->routine_model->get_selected_routine(array('user_id' => $user_id, 'type' => 'split'));
			// get all tracking_ids belonging to user and selecter routine
			$tracking_ids = $this->history_model->get_tracking_ids(array('selected_split_routine' => $selected_split_routine));
			foreach ($tracking_ids as $key => $value) {
				$this->history_model->insert_log(array('tracking_id' => $value['tracking_id'], 'weight' => $value['current_weight'], 'reps' => $value['user_reps'], 'sets' => $value['user_sets']));
			}
		} else {
			echo 'You clicked one of these buttons too recently. ' . $delta_human . ' Workout history logging is currently only possible once a day. ';
		}		
	// end new feature
		
		$split_routine_id = $this->routine_model->get_selected_routine(array('user_id' => $user_id, 'type' => 'split'));
		// $selected_split = $this->routine_model->get_selected_split(array('user_id' => $user_id, 'split_routine_id' => $split_routine_id));
		// get previous condition
		$previous_condition = $this->events_model->get_condition_id(array('user_id' => $user_id, 'split_routine_id' => $split_routine_id));
		// do new condition logic
		$abort_set = FALSE;  //UNSURE forgot adding this before, added on error, still need to check for bugs (try conditions)
		switch ($condition_id) {
			case NULL:
				echo 'This routine does not support automatic updating of values. '; //TODO make this a logo
				$abort_set = TRUE;
				// die;
				break;
			
			case '1':
				if ($previous_condition != 7) {
					$condition_id = $previous_condition;
					//XXX could offer advice to lower weight or take a break/reset here
				}
				break;
			//do logic: true: goto event 3, false: increase rep count
			case '2':
				// echo 'case 2 detected...';
				//check previous condition
				if ($previous_condition == 7) {
					// echo ' prev condition 7 detected ...';
					$this->run_condition(3); //HEE HEE HEE if you know what I say
					die; //XXX weird
				} else if (($previous_condition >= 4) && ($previous_condition <= 6)) {
					$condition_id = $previous_condition + 1;
				} else if ($previous_condition == 1) {
					// echo 'It seems as though you already pressed this button. ';
					$condition_id = 4;
				} else if ($previous_condition == 2) {
					echo 'Something went wrong with submitting your efforts last time, please contact the administrator if the site seems broken for you. '; //TODO error reporting
				}
				else {
					echo 'Unforseen condition/event error! Please contact the administrator. ';
				}
				break;
			
			//increase weight by 10%
			case '3':
				// echo 'test 3...';
				$routine_data = $this->events_model->get_split_routine_exercise_data(array('selected_split_routine' => $split_routine_id));
				// get tracking id for all exercises in routine FIXME
				foreach ($routine_data as $exercise) {
					// echo 'entered foreach loop ...';
					$tracking_id = $exercise['tracking_id'];
					$new_current_weight = ($exercise['current_weight'] * 1.1); 
					$this->routine_model->update_track_row($user_id, $tracking_id, NULL, $new_current_weight, NULL);
				}
				// $this->run_condition(1);
				// die;
				$condition_id = 1;
				break;
			default:
				echo 'Logical error: default case reached. ';
				$abort_set = TRUE;
				// die; //XXX is this die here good or bad?
				break;
		}
		// depending on logic: set new $condition id for submittal
		// echo ' ...test end reached...condition id:  ';
		// echo $condition_id;
		// die;
		if ($abort_set !== TRUE) {
			$result = $this->events_model->set_condition(array('condition_id' => $condition_id, 'user_id' => $user_id, 'split_routine_id' => $split_routine_id));
			if ($result >= 1) {
				echo 'To view the updated values, please refresh this page. ';
			} else {
				echo '';
				// echo 'It seems as though you already pressed a button. If you didn\'t, please contact us and tell us about this message. '; //XXX report error reporting
				// echo 'Our sincerest apologies; an error has occured. Tell us and we might just be able to fix it (error code: ajax:set_condition:query 0)!';  //XXX report error reporting
				
			}
		}
	}	
}


/* End of file ajax.php */
/* Location: .application/controllers/ajax.php */