<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Browse extends CI_Controller {
	function __construct() {
		parent::__construct();
		$this->load->model('routine_model');
	}
	public function index()
	{
		$data = array(
			'title' => 'YAOWT Browse routines',
			'content' => 'browse'
		);
		$user_id = check_logged_in();
		//FIXME the below is not DRY
		//show available routines (i.e. NOT already subbed routines)
		$subbed_routines = $this->routine_model->get_subbed_routines($user_id);
		$routine_ids = array();
		foreach ($subbed_routines as $key => $value) {
			array_push($routine_ids, $subbed_routines[$key]['routine_id']);
		}
		
		if (empty($routine_ids)) {
			$available_routines = $this->routine_model->get_all_routines();
		} else {
			$available_routines = $this->routine_model->get_available_routines($routine_ids);
		}

		$data['routines'] = array();
		foreach ($available_routines as $routine) {
			array_push($data['routines'], $routine);
		}

		/////// not DRY for other type of routines.........
		$subbed_split_routines = $this->routine_model->get_subbed_split_routines($user_id);
		$split_routine_ids = array();
		foreach ($subbed_split_routines as $key => $value) {
			array_push($split_routine_ids, $subbed_split_routines[$key]['split_routine_id']);
		}
		
		if (empty($split_routine_ids)) {
			$available_split_routines = $this->routine_model->get_all_split_routines();
		} else {
			$available_split_routines = $this->routine_model->get_available_split_routines($split_routine_ids);
		}

		$data['split_routines'] = array();
		foreach ($available_split_routines as $split_routine) {
			array_push($data['split_routines'], $split_routine);
		}

		$this->load->view('template', $data);
	}

	public function subscribe()
	{
		$user_id = check_logged_in();
		$routine_id = $this->input->post('routine_id'); 
		if (isset($routine_id)) {
			$this->session->set_flashdata('message', 'XXXXXXXX I SEE routine_id!' . print_r($routine_id));
		// } elseif (condition) {
// 			
		 } 
		else {
			$this->session->set_flashdata('message', 'I dont see it, cool!');
			//$this->session->set_flashdata('message', 'Subscription error 1, please report.');
		}
		$routine_id = $this->input->post('routine_id');
		//INSERT INTO yaowt_routine_tracking (user_id, routine_id) VALUES $user_id, $routine_id
		$this->routine_model->sub_routine($user_id, $routine_id);
		// $data = array(
			// 'user_id' => $user_id,
			// 'routine_id' => $routine_id
		// );
		// $this->db->insert('yaowt_routine_tracking', $data);
		$this->session->set_flashdata('message', 'You are now subscribed to: ' . $this->input->post('routine_name') . '.');
		redirect(base_url('browse'));
	}
	
	public function subscribe_split()
	{
		$user_id = check_logged_in();
		$split_routine_id = $this->input->post('split_routine_id'); 
		
		$this->routine_model->sub_split($user_id, $split_routine_id);
		// $data = array(
			// 'user_id' => $user_id,
			// 'routine_id' => $split_routine_id
		// );
		// $this->db->insert('yaowt_split_routine_tracking', $data);
		$this->session->set_flashdata('message', 'You are now subscribed to: ' . $this->input->post('split_routine_name') . '.');
		redirect(base_url('browse'));
	}
}

/* End of file browse.php */
/* Location: .application/controllers/browse.php */