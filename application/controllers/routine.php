<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Routine extends CI_Controller {	
	function __construct() {
		parent::__construct();
		$this->load->model('routine_model');
	}
	public function index()
	{
		$data['messages'] = array();
		
		// if logged in display user related table, else display generic table
		//$user_id2 = $this->ion_auth->user()->row();
		//TODO make this user check a function on its own? (check_logged_in is slightly different)
		//    but if you want to prevent nog logged in users from doing anything (but login/signup, then use that function here instead)
		if ($this->ion_auth->user()->row()) {		
			$user_id = $this->ion_auth->user()->row()->id;
			//echo 'ID: ' . $user_id;
			//else if not logged in:
		} else {
			$user_id = 1; // set this to an example user
			//echo 'not logged in!';
		}
				
		if ($this->routine_model->count_subs($user_id) == 0) {
			//user is not subscribed to any routines, set user_id to example user: 1
			$user_id = 1;
		} 
		
		$this->routine_model->count_subs($user_id);
		
		$check_selected_message = $this->routine_model->check_selected_routine($user_id);
		array_push($data['messages'], $check_selected_message);
				
		$selected_routine = $this->routine_model->get_selected_routine(array('user_id' => $user_id));
		
				
		$check_start_message = $this->routine_model->check_starting_date($user_id, $selected_routine);
		array_push($data['messages'], $check_start_message);

		$routine_name = $this->routine_model->get_routine_name($user_id, $selected_routine);

		$data['title'] = 'YAOWT Track your workout';
		$data['content'] = 'routine';
		$data['routine_name'] = $routine_name;
		$data['parser_data']['day_type'] = $this->routine_model->get_day_type($user_id, $selected_routine);
		
		

		// set a message if example routine is being shown
		if ($data['routine_name'] == 'This is an example routine') { //XXX note that this is dependent on a string set in DB
			//print_r($data);
			//echo gettype($data['messages']);
			array_push($data['messages'], '<div class="message">It appears you\'ve not yet chosen a routine. Please visit the <a href="' . base_url('profile') . '">profile page</a> to pick one.</div>');
		}
				
		//run from here if there are missing rows in yaowt_tracking
		if ($selected_routine == 1) {  //OLD: if (empty($result)) {
			// set user_id to 1 (example user TODO FIXME make sure in the production version that it isnt an admin user)
			$user_id = 1;
		}
		
		$data['parser_data']['exercises'] = $this->routine_model->get_exercises($user_id, $selected_routine, $data['parser_data']['day_type']); //TODO see question QX1 (added last arg)
		
		$user_id = check_logged_in();
				
		//check for missing tracking rows
		$routine_compo = $this->routine_model->get_routine_composition($selected_routine);
		// echo count($data['parser_data']['exercises']);
		// echo '<br />';
		// echo count($routine_compo);
		
		// print_r($data['parser_data']['exercises']);
		// die;
		

		$routine_compo_ids = array(); ///XXX ugly, lambda filter function or something?
		foreach ($data['parser_data']['exercises'] as $exercise) {
			array_push($routine_compo_ids, $exercise['routine_composition_id']);
		}
		
		// does tracking table have all r_c_id's stored in $r_compo_ids?
		//tracking table ids: $data['parser_data']['exercises'][$key]['routine_composition_id']
		foreach ($routine_compo as $exercise) {
			if (!in_array($exercise['routine_composition_id'], $routine_compo_ids)) {
				//add the missing tracking data, based on user_id and routine_compo_id
				$routine_compo_id = $exercise['routine_composition_id'];
				$this->routine_model->add_missing_tracking_row(array('user_id' => $user_id, 'routine_compo_id' => $routine_compo_id, 'type' => 'routine'));
			}
		}
		//reload data which depends on tracking data:
		$data['parser_data']['exercises'] = $this->routine_model->get_exercises($user_id, $selected_routine, $data['parser_data']['day_type']); //XXX DRY
		// $routine_compo = $this->routine_model->get_routine_composition($selected_routine); //doesnt seem like this needs refreshing
		
		// if (count($routine_compo) > count($data['parser_data']['exercises'])) {
			// $this->routine_model->populate_empty_weight($user_id, $selected_routine);
			// $data['parser_data']['exercises'] = $this->routine_model->get_exercises($user_id, $selected_routine, $data['parser_data']['day_type']); //XXX DRY
			// $routine_compo = $this->routine_model->get_routine_composition($selected_routine);
			// //what did this update? the tracking table. what in this method uses the tracking table?
			// // array_push($data['messages'], $populate_empty_message);
		// } ///how does data get updated???? because the model had a redirect in it.. that should not be there!
		
		//print_r($data['parser_data']['exercises']);
		
		// foreach ($data['parser_data']['exercises'] as $key => $value) {
			// //echo 'key: ' . $key . ' value: ' .  print_r($value);
		// }
		
		//maybe the conditional below is superflous because of the conditional above?
		// if (empty($data['parser_data']['exercises'])) {
			// //echo 'its empty alright';
			// $this->routine_model->populate_empty_weight($user_id, $selected_routine);
		// }
		//send the start_date data to the view
		// put the below in the model
		$start_date = $this->routine_model->get_routine_start_date($user_id, $selected_routine);
		// $this->db->from('yaowt_routine_tracking');
		// $this->db->where('user_id', $user_id);
		// $this->db->where('routine_id', $selected_routine);
		// //print_r($this->db->get()->result());
		// $start_date = $this->db->get()->row()->starting_date;
		
		//TODO check checking conditions (no start date, no user)
		//$data['start_date'] = array(); //ugly?
		$data['start_date'] = $start_date;
		//echo $start_date;
		
		$this->load->view('template', $data);
		//$this->parser->parse('template', $data);
		//$this->parser->parse('routine', $data);
		//$this->load->view('routine');
		
		
	}
	
	private function start_routine_now()
	{
		$user_id = check_logged_in();
		// if ($this->ion_auth->user()->row()) {		
			// $user_id = $this->ion_auth->user()->row()->id;
		// } else {
			// //echo 'not logged in!';
			// redirect(base_url());
		// }
		$selected_routine = $this->routine_model->get_selected_routine(array('user_id' => $user_id));
		$this->routine_model->set_routine_start_now(array('user_id' => $user_id, 'selected_routine' => $selected_routine));
		//echo '<br />TEST start routine now<br />';
		redirect(base_url('routine'));
	}
	
	// update the weight columns in the tracking table
	private function update_weight()
	{
		$user_id = check_logged_in();
		$selected_routine = $this->routine_model->get_selected_routine(array('user_id' => $user_id));
		$data['parser_data']['day_type'] = $this->routine_model->get_day_type($user_id, $selected_routine);
		
		
		$data['parser_data']['exercises'] = $this->routine_model->get_exercises($user_id, $selected_routine, $data['parser_data']['day_type']);
		//echo count($data['parser_data']['exercises']);
		//echo 'YOYOYOYO';
		
		// {
			
		//}
		
		foreach ($data['parser_data']['exercises'] as $key => $value) {
			// TODO add more validation?
			$tracking_id = $this->input->post($key);;
			$new_start_weight = $this->input->post($tracking_id . '_S');
			$new_current_weight = $this->input->post($tracking_id . '_C');;
			// throw errors if the variables above aren't set IMPORTANT!!!!! (changed to: update only values with numbers)
			// throw errors if variables aren't numbers (allow input only number possible with HTML? or use validator)(as above)			
			if ((is_numeric($new_start_weight) || ($new_start_weight == NULL)) && is_numeric($new_current_weight)) {
				$this->routine_model->update_track_row($user_id, $tracking_id, $new_start_weight, $new_current_weight);
			}
		}
		
		redirect(base_url('routine'));
	}

	private function update_startdate()
	{
		$start_date = $this->input->post('datetime');
		//FIXME IMPORTANT! validate $start_date (inser_string escapes the query so it should be ok right? yes: http://stackoverflow.com/questions/2401706/
		$user_id = check_logged_in(); //TODO disable update button when not logged in
		//TODO check if submitted start date differs from the original start date (use a hidden field, seems much cheaper than issuing another query)
		$routine_id = $this->routine_model->get_selected_routine(array('user_id' => $user_id));
		//XXX move to model
		//INSERT INTO yaowt_routine_tracking user_id, routine_id, starting_date VALUES ($user_id, $routine_id, $start_date) 
		//	ON DUPLICATE KEY UPDATE starting_date = $start_date (is the last part ok?)
		$data = array(
			'user_id' => $user_id, 
			'routine_id' => $routine_id, 
			'starting_date' => $start_date
		);
		$sql = $this->db->insert_string('yaowt_routine_tracking', $data) . ' ON DUPLICATE KEY UPDATE starting_date = "'. $start_date . '"';
		//routine_tracking_id = routine_tracking_id + 2'
		$this->db->query($sql);
		redirect(base_url('routine'));
	}
	
}


/* End of file routine.php */
/* Location: .application/controllers/routine.php */