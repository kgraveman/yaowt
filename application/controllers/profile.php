<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Profile extends CI_Controller {
	function __construct() {
		parent::__construct();
		$this->load->model('routine_model');
	}
	public function index()
	{
			$data = array(
				'title' => 'YAOWT Profile',
				'content' => 'profile'
		);
		$user_id = check_logged_in();
		// if ($this->ion_auth->user()->row()) {		
			// $user_id = $this->ion_auth->user()->row()->id;
			// //echo 'ID: ' . $user_id;
			// //elseif not logged in:
		// } else {
			// redirect(base_url()); // set this to an example user (no, just redirect users to login/signup)
			// //die();
		// }
		
		// get subscribed routines and Check if the current user is subscribed to at least one routine, (that function should otherwise return the example routine)
		// and pass that data to the view
		
		if ($this->routine_model->count_subs($user_id) == 0) {
			//echo 'No subs!';
			//sub user to example routine (1)
			$this->routine_model->sub_routine($user_id, 1); // XXX note that 1 is the example routine
		}
		
		$this->routine_model->check_selected_routine($user_id);
		//if subbed to at least one routine:
		if ($this->routine_model->count_split_routine_subs($user_id) >= 1) {
			//check and set if nothing is set
			$this->routine_model->check_selected_split_routine($user_id);
		}
		
		
		$subbed_routines = $this->routine_model->get_subbed_routines($user_id);
		$subbed_routines = array_merge($subbed_routines, $this->routine_model->get_subbed_split_routines($user_id));
		if (!empty($subbed_routines)) {
			for ($i=0; $i < count($subbed_routines); $i++) {
				
				// adding regular routine data to be passed //XXX DRY
				if (isset($subbed_routines[$i]['routine_id'])) {
					$data['parser_data']['routines'][$i] = array('routine_id' => $subbed_routines[$i]['routine_id'], 'routine_name' => $subbed_routines[$i]['routine_name']);
					$routine_id = $subbed_routines[$i]['routine_id'];
					$is_split_routine = FALSE;
					
					$is_selected = $this->routine_model->check_selected($user_id, $routine_id, $is_split_routine);
					if ($is_selected == TRUE) {
						$data['parser_data']['routines'][$i]['selected'] = ' selected';
					} else {
						$data['parser_data']['routines'][$i]['selected'] = NULL;
					}
				}
				// adding split routine data to be passed
				if (isset($subbed_routines[$i]['split_routine_id'])) {
					$data['parser_data']['split_routines'][$i] = array('split_routine_id' => $subbed_routines[$i]['split_routine_id'], 'split_routine_name' => $subbed_routines[$i]['routine_name']);
					$routine_id = $subbed_routines[$i]['split_routine_id'];
					$is_split_routine = TRUE;
					
					$is_selected = $this->routine_model->check_selected($user_id, $routine_id, $is_split_routine);
					if ($is_selected == TRUE) {
						$data['parser_data']['split_routines'][$i]['selected'] = ' selected';
					} else {
						$data['parser_data']['split_routines'][$i]['selected'] = NULL;
					}
				}

			}
		} else {
			//echo 'YOU ARE ROUTINELESS!';
			$data['parser_data']['routines'][0] = array('routine_id' => 0, 'routine_name' => 'Please subscribe to a routine', 'selected' => ' selected'); //XXX note that 0 is no routine!
		}
		
		
		$this->load->view('template', $data);
		
		
	}
	
	//
	public function routine()
	{
		$user_id = check_logged_in();
		// get current users id TODO this is a duplicate, move to own function?
		// if ($this->ion_auth->user()->row()) {		
			// $user_id = $this->ion_auth->user()->row()->id;
			// //echo 'ID: ' . $user_id;
			// //elseif not logged in:
		// } else {
// 			
			// redirect(base_url()); // set this to an example user
			// //die();
		// }
	
		// TODO form validation
		$action = $this->input->post('routine_btn');
		if ($action === FALSE) {
			show_404();
		}
		$form_selected_routine = $this->input->post('routines');
		
		//'select';
		if ($action == 'Select') {
			if ($this->input->post('routines') == "disabled") { //XXX DRY
				$this->session->set_flashdata('alert', 'Selecting and removing empty routine disabled.');
				redirect(base_url('profile'));
			}
			
			
			if ($this->input->post('is_split') == 'yes') {
				$this->routine_model->set_selected_split_routine($user_id, $form_selected_routine);
			} else {
				$this->routine_model->set_selected_routine($user_id, $form_selected_routine);
			}
		//'removing';
		} elseif ($action == 'Remove') {
			if ($this->input->post('routines') == "disabled") {
				$this->session->set_flashdata('alert', 'Selecting and removing empty routine disabled.');
				redirect(base_url('profile'));
			}

			
			//check if trying to remove example routine if its the only subbed routine
			if (($form_selected_routine == 1) && ($this->routine_model->count_subs($user_id) == 1)) { //FIXME minor bug: what if last split has id 1?
				
				$this->session->set_flashdata('alert', 'Please subscribe to another routine before deleting the example routine.');
				redirect(base_url('profile'));
				
			}
			if ($this->input->post('is_split') == 'yes') {
				$this->routine_model->unsub_split_routine($user_id, $form_selected_routine);
				//check if the routine we remove was the selected routine, if so, unset the selected split routine field in db ($this->index will reset it)
				$selected_split_routine = $this->routine_model->get_selected_routine(array('user_id' => $user_id, 'type' => 'split'));
				if ($form_selected_routine == $selected_split_routine) {
					$this->routine_model->rem_split_routine_selected($user_id);
				}
				//the if conditional below becomes superfluous because of the one above...
				//but if resetting somehow fails, this may prevent bugs, so its not superfluous
				if ($this->routine_model->count_split_routine_subs($user_id) == 0){
					// echo "<script>alert('that was the last one')</script>";
					//now remove the selected
					$this->routine_model->rem_split_routine_selected($user_id);
				}
			} else {
				$this->routine_model->unsub_routine($user_id, $form_selected_routine);	
			}
		} elseif ($action="Browse routines") {
			redirect(base_url('browse'));
		}
		redirect(base_url('profile'));
	}
		
}




/* End of file profile.php */
/* Location: .application/controllers/profile.php */