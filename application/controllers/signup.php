<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Signup extends CI_Controller {
	function __construct() {
		parent::__construct();
	}
	public function index()
	{
		
		redirect(base_url() . 'auth/user_registration');
		die();
		// below no longer required?!
		$this->load->library('form_validation');
		
		$this->form_validation->set_rules('username', 'Username', 'trim|required|min_length[5]|max_length[20]|is_unique[users.username]|xss_clean');
		$this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[6]|max_length[50]|matches[password2]|xss_clean');
		$this->form_validation->set_rules('password2', 'Password confirmation', 'trim|required|min_length[5]|max_length[50]|xss_clean');
		$this->form_validation->set_rules('email1', 'Email', 'trim|required|min_length[2]|max_length[50]|is_unique[users.email]|valid_email|xss_clean');
		$this->form_validation->set_rules('email', 'Sweet', 'callback_email_check');
		
		if ($this->form_validation->run() == FALSE) {
			$data = array(
				'title' => 'Signup',
				'content' => 'signup'
			);
		$this->load->view('template', $data);
		} else {
			$this->load->view('signupsuccess');
		}
		
		
		
	}
	
	public function email_check($str)
	{
		check_post();
		if ($str == '') {
			return TRUE;
		} else {
			return FALSE;
		}
		
	}
}

/* End of file signup.php */
/* Location: .application/controllers/signup.php */