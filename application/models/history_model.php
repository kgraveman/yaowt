<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class History_model extends CI_Model {
	function __construct() {
		parent::__construct();
		
	}
	public function insert_log($options = array())
	{
		if (_required(array('tracking_id'), $options) == FALSE) {
			return FALSE;
		}
		
		$data = array(
			'tracking_id' => $options['tracking_id'],
			'weight' => $options['weight'],
			'reps' => $options['reps'],
			'sets' => $options['sets']
		);
		$this->db->insert('yaowt_tracking_history', $data);
	}
	
	public function get_tracking_ids($options = array())
	{
		if (_required(array('selected_split_routine'), $options) == FALSE) {
			return FALSE;
		}
		// $selected_split_routine == y_spl_r_compo.split_routine_id -> split_ids ->  [split_compo].split_compo_ids -> tracking_ids
		$this->db->select('tracking_id, current_weight, user_reps, user_sets');
		$this->db->from('yaowt_tracking');
		$this->db->join('yaowt_split_composition', 'yaowt_tracking.split_composition_id = yaowt_split_composition.split_composition_id');
		$this->db->join('yaowt_split_routine_composition', 'yaowt_split_composition.split_id = yaowt_split_routine_composition.split_id');
		$this->db->where('yaowt_split_routine_composition.split_routine_id', $options['selected_split_routine']);
		return $this->db->get()->result_array();
		
	}
	
	public function get_history($options = array())
	{
		if (_required(array('user_id', 'selected_split_routine'), $options) == FALSE) {
			return FALSE;
		}
		//yaowt_selected_routine.split_routine_id-> yaowt_split_routine_composition.split_routine_id  -> 
		
		// yaowt_split_composition.split_id = 
		
		//	yaowt_split_composition.split_composition_id = yaowt_tracking.split_composition_id) -> 
		//	yaowt_tracking.tracking_id -> yaowt_tracking_history.tracking_id
		
		
		//(yaowt_split_routine_composition.split_composition_id
		$this->db->select('tracking_timestamp, weight, reps, sets');
		$this->db->from('yaowt_tracking_history');
		$this->db->join('yaowt_tracking', 'yaowt_tracking_history.tracking_id = yaowt_tracking.tracking_id');
		$this->db->join('yaowt_split_composition', 'yaowt_tracking.split_composition_id = yaowt_split_composition.split_composition_id');
		$this->db->join('yaowt_split_routine_composition', 'yaowt_split_composition.split_id = yaowt_split_routine_composition.split_id');
		$this->db->join('yaowt_selected_routine', 'yaowt_split_routine_composition.split_routine_id = yaowt_selected_routine.split_routine_id');
		$this->db->where('yaowt_tracking.user_id', $options['user_id']);
		$this->db->where('yaowt_selected_routine.split_routine_id', $options['selected_split_routine']);
		return $this->db->get()->result_array();
		
	}

	public function get_exercise_history($options = array())
	{
		if (_required(array('exercise_id', 'user_id', 'routine_id'), $options) == FALSE) {
			return FALSE;
		}
		$this->db->select('tracking_timestamp, weight, reps, sets');
		$this->db->from('yaowt_tracking_history');
		$this->db->join('yaowt_tracking', 'yaowt_tracking_history.tracking_id = yaowt_tracking.tracking_id');
		$this->db->join('yaowt_split_composition', 'yaowt_tracking.split_composition_id = yaowt_split_composition.split_composition_id');
		$this->db->join('yaowt_split_routine_composition', 'yaowt_split_composition.split_id = yaowt_split_routine_composition.split_id');
		$this->db->join('yaowt_selected_routine', 'yaowt_split_routine_composition.split_routine_id = yaowt_selected_routine.split_routine_id');
		$this->db->where('yaowt_tracking.user_id', $options['user_id']);
		$this->db->where('yaowt_selected_routine.split_routine_id', $options['routine_id']);
		$this->db->where('yaowt_split_composition.exercise_id', $options['exercise_id']);
		return $this->db->get()->result_array();
	}

	public function get_exercise_ids($options = array())
	{
		if (_required(array('selected_routine'), $options) == FALSE) {
			return FALSE;
		}
		$this->db->select('exercise_id');
		$this->db->from('yaowt_split_composition');
		$this->db->join('yaowt_split_routine_composition', 'yaowt_split_composition.split_id = yaowt_split_routine_composition.split_id');
		$this->db->join('yaowt_selected_routine', 'yaowt_split_routine_composition.split_routine_id = yaowt_selected_routine.split_routine_id');
		$this->db->where('yaowt_selected_routine.split_routine_id', $options['selected_routine']);
		return $this->db->get()->result_array();
	}
	
	public function get_exercise_name($options = array())
	{
		if (_required(array('exercise_id'), $options) == FALSE) {
			return FALSE;
		}
		$this->db->select('exercise_name');
		$this->db->from('yaowt_exercises');
		$this->db->where('exercise_id', $options['exercise_id']);
		return $this->db->get()->row()->exercise_name;
	}

	public function check_history_data($options = array())
	{
		//tracking_id -> split_composition_id ->     split_id ->    y_spl_r_compo -> y_selected_routine.split_routine_id
		//where selected AND user(tracking)
		if (_required(array('selected_routine', 'user_id'), $options) == FALSE) {
			return FALSE;
		}
		$this->db->from('yaowt_tracking_history');
		$this->db->join('yaowt_tracking', 'yaowt_tracking_history.tracking_id = yaowt_tracking.tracking_id');
		$this->db->join('yaowt_split_composition', 'yaowt_tracking.split_composition_id = yaowt_split_composition.split_composition_id');
		$this->db->join('yaowt_split_routine_composition', 'yaowt_split_composition.split_id = yaowt_split_routine_composition.split_id');
		$this->db->join('yaowt_selected_routine', 'yaowt_split_routine_composition.split_routine_id = yaowt_selected_routine.split_routine_id');
		$this->db->where('yaowt_selected_routine.split_routine_id', $options['selected_routine']);
		$this->db->where('yaowt_tracking.user_id', $options['user_id']);
		return $this->db->count_all_results();
	}
	

}


/* End of file history_model.php */
/* Location: .application/controllers/history_model.php */