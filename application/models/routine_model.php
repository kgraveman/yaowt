<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Routine_model extends CI_Model {
	function __construct() {
		parent::__construct();
	}
	
	// OLD:if a user is not subscribed to any routines, --stop some other functions, show example routine (user_id=1)
	// NEW: return to how many routines a user is subscribed
	public function count_subs($user_id) //FIXME rename this and dependencies to count_routine_subs
	{
		// SELECT * FROM yaowt_routine_tracking WHERE user_id = $user_id
		// count_all_results()
		$this->db->from('yaowt_routine_tracking');
		$this->db->where('user_id', $user_id);
		//return $this->db->count_all_results(); // is this ok?
		$result = $this->db->count_all_results();
		return $result;
	}
	
	public function count_split_routine_subs($user_id)
	{
		$this->db->from('yaowt_split_routine_tracking');
		$this->db->where('user_id', $user_id);
		//return $this->db->count_all_results(); // is this ok?
		$result = $this->db->count_all_results();
		return $result;
	}
	
	public function count_splits_in_routine($options = array())
	{
		if (_required(array('split_routine_id'), $options) == FALSE) {
			return FALSE;
		}
		$this->db->from('yaowt_split_routine_composition');
		$this->db->where('split_routine_id', $options['split_routine_id']);
		return $this->db->count_all_results();
	}
	
	// check if a user has set a (any) routine as selected, if not, set the first found as selected this should normally never happen because: ..?
	// TODO set the first subbed routine as selected; ask if newly subbed routines should be set as selected
	public function check_selected_routine($user_id)
	{
		//SELECT routine_id FROM yaowt_selected_routine WHERE user_id = $user_id
		$this->db->select('routine_id');
		$this->db->from('yaowt_selected_routine');
		$this->db->where('user_id', $user_id);
		
		if (empty($this->db->get()->row()->routine_id)) { //look for empty as in split_routine below, also use ON DUPLICATE KEY
			//echo $user_id;
			//SELECT routine_id FROM yaowt_routine_tracking WHERE user_id = $user_id
			$this->db->select('routine_id');
			$this->db->from('yaowt_routine_tracking');
			$this->db->where('user_id', $user_id);
			// $query = $this->db->get();
			// $any_routine = $query->row()->routine_id; // OO vs array? benchmark says identical: 0.0004
			$any_routine = $this->db->get()->row()->routine_id;
			//INSERT INTO yaowt_selected_routine (user_id, routine_id) VALUES ($user_id, $any_routine) // TODO instead of any routine: dialog box
			$data = array(
				'user_id' => $user_id,
				'routine_id' => $any_routine
			);
			$sql = $this->db->insert_string('yaowt_selected_routine', $data) . ' ON DUPLICATE KEY UPDATE routine_id = "' . $any_routine . '"';
			//$this->db->insert('yaowt_selected_routine', $data);
			$this->db->query($sql);
			return 'To select another routine go to the <a href="' . base_url('profile') . '">profile page</a>.';
		}
	}
	//XXX DRY, and too much logic
	public function check_selected_split_routine($user_id)
	{
		//SELECT routine_id FROM yaowt_selected_routine WHERE user_id = $user_id
		$this->db->select('split_routine_id');
		$this->db->from('yaowt_selected_routine');
		$this->db->where('user_id', $user_id);
		
		if (empty($this->db->get()->row()->split_routine_id)) {
			// echo '<script>alert("test")</script>';
			//SELECT routine_id FROM yaowt_routine_tracking WHERE user_id = $user_id
			$this->db->select('split_routine_id');
			$this->db->from('yaowt_split_routine_tracking');
			$this->db->where('user_id', $user_id);
			$query = $this->db->get();
			$any_routine = $query->row()->split_routine_id; // OO vs array?
			
			//INSERT INTO yaowt_selected_routine (user_id, routine_id) VALUES ($user_id, $any_routine) // TODO instead of any routine: dialog box
			$data = array(
				'user_id' => $user_id,
				'split_routine_id' => $any_routine
			);
			$sql = $this->db->insert_string('yaowt_selected_routine', $data) . ' ON DUPLICATE KEY UPDATE split_routine_id = "' . $any_routine . '"';
			// $this->db->insert('yaowt_selected_routine', $data);
			$this->db->query($sql);
			return 'To select another routine go to the <a href="' . base_url('profile') . '">profile page</a>.';
		}
	}
	
	//returns the routine_id that the user has selected. takes user_id, type
	public function get_selected_routine($options = array())
	{
		//check for required values
		if (_required(array('user_id'), $options) == FALSE) {
			return FALSE;
		}
		//default values
		$options = _default(array('type' => ''), $options);
		
		// TODO check that other functions dont do this internally (duplication)
		// SELECT routine_id FROM yaowt_selected_routine WHERE user_id = $user_id
		$this->db->select('yaowt_selected_routine.' . routine_type($options['type']) . 'routine_id');
		$this->db->from('yaowt_selected_routine');
		$this->db->where('user_id', $options['user_id']);
		//$query = $this->db->get();
		//$result = $query->result_array(); //TODO whats faster? this or OO notation?
		$result = $this->db->get()->row_array(); //$result = $this->db->get()->result_array();
		if (!empty($result)) {
			return $result[routine_type($options['type']) . 'routine_id']; //return $result[0][routine_type($options['type']) . 'routine_id'];
		}
	}

	public function get_selected_split($options = array())
	{
		//required:
		if (_required(array('user_id', 'split_routine_id'), $options) == FALSE) {
			return FALSE;
		}
		$this->db->select('selected_split_id');
		// $this->db->from('yaowt_selected_routine');
		$this->db->from('yaowt_split_routine_tracking');
		$this->db->where('user_id', $options['user_id']);
		$this->db->where('split_routine_id', $options['split_routine_id']);
		$result = $this->db->get()->row_array();
		return $result['selected_split_id'];
	}
	
	public function get_split_by_number($options = array())
	{
		if (_required(array('split_number', 'selected_split_routine'), $options) == FALSE) {
			return FALSE;
		}
		
		// echo $options['split_number'];
		// die;
		// $this->db->select('split_id');
		// $this->db->from('yaowt_split_routine_composition');
		// $this->db->where('split_routine_id', $options['selected_split_routine']);
		// $this->db->get()->result_array();
		$this->db->select('yaowt_splits.split_id');
		$this->db->from('yaowt_splits');
		$this->db->join('yaowt_split_routine_composition', 'yaowt_splits.split_id = yaowt_split_routine_composition.split_id');
		// $this->db->where('yaowt_splits.split_id', );
		$this->db->where('split_routine_id', $options['selected_split_routine']);
		$this->db->where('yaowt_splits.split_number', $options['split_number']);		
		return $split_id = $this->db->get()->row()->split_id;

	}
	
	//It seems i fixed this already? this might work, but depends on successive numbering. to be safe, get max of day number and then join tables
	public function get_final_split($options = array())
	{
		if (_required(array('selected_split_routine'), $options) == FALSE) {
			return FALSE;
		}
		$this->db->select_max('split_number');
		$this->db->from('yaowt_splits');
		$this->db->join('yaowt_split_routine_composition', 'yaowt_splits.split_id = yaowt_split_routine_composition.split_id');
		$this->db->where('split_routine_id', $options['selected_split_routine']);
		$split_number = $this->db->get()->row()->split_number;
		
		$this->db->select('yaowt_splits.split_id');
		$this->db->from('yaowt_splits');
		$this->db->join('yaowt_split_routine_composition', 'yaowt_splits.split_id = yaowt_split_routine_composition.split_id');
		//these where and join statements were missing
		$this->db->where('split_routine_id', $options['selected_split_routine']);
		$this->db->where('split_number', $split_number);
		return $this->db->get()->row()->split_id;		
	}
	
	public function get_first_split($options = array())
	{
		if (_required(array('selected_split_routine'), $options) == FALSE) {
			return FALSE;
		}
		// $this->db->select_min('split_number');
		// $this->db->from('yaowt_splits');
		// $this->db->join('yaowt_split_routine_composition', 'yaowt_splits.split_id = yaowt_split_routine_composition.split_id');
		// $this->db->where('split_routine_id', $options['selected_split_routine']);
		// $split_number = $this->db->get()->row()->split_number;
		$split_number = 1;
		
		$this->db->select('yaowt_splits.split_id');
		$this->db->from('yaowt_splits');
		$this->db->join('yaowt_split_routine_composition', 'yaowt_splits.split_id = yaowt_split_routine_composition.split_id');
		$this->db->where('split_routine_id', $options['selected_split_routine']);
		$this->db->where('split_number', $split_number);
		return $this->db->get()->row()->split_id;
	}
	
	//based on split number
	public function set_selected_split($options = array())
	{
		if (_required(array('user_id'/*, 'split_number'*/, 'split_routine_id', 'new_split_id'), $options) == FALSE) {
			return FALSE;
			
		}
		//get highest split_id
		// $this->db->select('yaowt_splits.split_id');
		// $this->db->from('yaowt_splits');
		// $this->db->where('yaowt_splits.split_number', $options['split_number']);
		// $new_split_id = $this->db->get()->row()->split_id;
		$data = array(
			'selected_split_id' => $options['new_split_id']
		);
		// $this->db->where('yaowt_selected_routine.user_id', $options['user_id']);
		// $this->db->update('yaowt_selected_routine', $data);
		$this->db->where('yaowt_split_routine_tracking.user_id', $options['user_id']);
		$this->db->where('split_routine_id', $options['split_routine_id']);
		// $this->db->update('yaowt_selected_routine', $data);
		$this->db->update('yaowt_split_routine_tracking', $data);
	}
	
	public function set_manual_split_view($options = array())
	{
		if (_required(array('user_id', 'bool', 'split_routine_id'), $options) == FALSE) {
			return FALSE;
		}
		
		$this->db->where('user_id', $options['user_id']);
		$this->db->where('split_routine_id', $options['split_routine_id']);
		$data = array(
			'view_split_manually' => $options['bool']
		);
		$this->db->update('yaowt_split_routine_tracking', $data);
	}
	
	public function is_manual_split_view($options = array())
	{
		if (_required(array('user_id', 'split_routine_id'), $options) == FALSE) {
			return FALSE;
		}
		
		$this->db->select('view_split_manually');
		$this->db->from('yaowt_split_routine_tracking');
		$this->db->where('user_id', $options['user_id']);
		$this->db->where('split_routine_id', $options['split_routine_id']);
		return $this->db->get()->row()->view_split_manually;
	}
	
	// public function get_selected_split_routine($user_id)
	// {
		// $this->db->select('yaowt_selected_routine.split_routine_id');
		// $this->db->from('yaowt_selected_routine');
		// $this->db->where('user_id', $user_id);
		// $result = $this->db->get()->result_array();
		// //$result = $query->result_array(); //whats faster? this or procedural with an extra assignment? this eh 
		// return $result[0]['split_routine_id'];		// what about using row_array?
	// }
	
	public function set_selected_routine($user_id, $form_selected_routine)
	{
		// UPDATE yaowt_selected_routine SET routine_id = $form_selected_routine WHERE user_id = $user_id
		$this->db->set('routine_id', $form_selected_routine);
		$this->db->where('yaowt_selected_routine.user_id', $user_id);
		$this->db->update('yaowt_selected_routine');  // NOTE that update must come last
				
	}

	public function set_selected_split_routine($user_id, $form_selected_routine)
	{
		// echo '<script>alert("' . $form_selected_routine. '")</script>';
		// die;
		$this->db->set('split_routine_id', $form_selected_routine);
		$this->db->where('yaowt_selected_routine.user_id', $user_id);
		$this->db->update('yaowt_selected_routine');  // NOTE that update must come last
	}
	
	//moved to events model
	// public function set_condition($options = array())
	// {
		// if (_required(array('condition_id', 'user_id', 'split_routine_id'), $options) == FALSE) {
			// return FALSE;
		// }
// 		
		// $this->db->where('user_id', $options['user_id']);
		// $this->db->where('split_routine_id', $options['split_routine_id']);
		// $data = array(
			// 'condition_id' => $options['condition_id']
		// );
		// $this->db->update('yaowt_split_routine_tracking', $data);
		// return $this->db->affected_rows();
	// }
	
	public function rem_split_routine_selected($user_id)
	{
		$data = array('split_routine_id' => NULL);
		$this->db->where('user_id', $user_id);
		$this->db->update('yaowt_selected_routine', $data);
	}
	
	//takes $user_id, $selected_routine, type
	public function set_routine_start_now($options = array())
	{
		//check for required values
		if (_required(array('user_id', 'selected_routine'), $options) == FALSE) {
			return FALSE;
		}
		//default values
		$options = _default(array('type' => ''), $options);
		
		// $format = 'Y-m-d H:i:s'; //Old local style
		// $current_date = date($format);
		
		//$current_date = "insert_time=now()";
		// UPDATE yaowt_routine_tracking SET starting_date = $current_date WHERE user_id = $user_id AND routine_id = $selected_routine
		$this->db->set('starting_date', 'NOW()', FALSE);
		$this->db->where('user_id', $options['user_id']);
		$this->db->where(routine_type($options['type']) . 'routine_id', $options['selected_routine']);
		$this->db->update('yaowt_' . routine_type($options['type']) . 'routine_tracking');
	}
	
	
	// checks if a starting date is present for the currently logged in user and his/her selected routine
	// returns data to be parsed by view if no starting date was found
	//FIXME too much logic
	public function check_starting_date($user_id, $selected_routine)
	{
		// TODO (duplicate but important) prevent user from subscribing to more than 1 of the same routine
		// SELECT starting_date FROM yaowt_routine_tracking JOIN yaowt_selected_routine ON yaowt_routine_tracking.routine_id = yaowt_selected_routine.routine_id WHERE user_id = $user_id AND selected_routine = $selected_routine
		$this->db->select('starting_date');
		$this->db->from('yaowt_routine_tracking');
		//$this->db->join('yaowt_selected_routine', 'yaowt_routine_tracking.routine_id = yaowt_selected_routine.routine_id');
		$this->db->where('user_id', $user_id);
		$this->db->where('routine_id', $selected_routine);
		$query = $this->db->get();
		$result = $query->result_array();

		if (empty($result[0]['starting_date'])) {
			//echo "no start date found <br />";
			// YES sets start_date to now
			// NO returns to overview page 
			$data =	'You haven\'t set a starting date for this routine. <a href="routine/start_routine_now">Click here to start tracking this routine now.</a><br />
				The weight values below are not adjusted according to the routine\'s specifications.';
			
			return $data;
		}
	}
	//FIXME DRY, too much logic
	public function check_split_routine_starting_date($user_id, $selected_routine)
	{
		// TODO (duplicate but important) prevent user from subscribing to more than 1 of the same routine
		// SELECT starting_date FROM yaowt_routine_tracking JOIN yaowt_selected_routine ON yaowt_routine_tracking.routine_id = yaowt_selected_routine.routine_id WHERE user_id = $user_id AND selected_routine = $selected_routine
		$this->db->select('starting_date');
		$this->db->from('yaowt_split_routine_tracking');
		//$this->db->join('yaowt_selected_routine', 'yaowt_routine_tracking.routine_id = yaowt_selected_routine.routine_id');
		$this->db->where('user_id', $user_id);
		$this->db->where('split_routine_id', $selected_routine);
		$query = $this->db->get();
		$result = $query->result_array();

		if (empty($result[0]['starting_date'])) {
			//echo "no start date found <br />";
			// YES sets start_date to now
			// NO returns to overview page 
			$data =	'You haven\'t set a starting date for this routine. <a href="split_routine/start_routine_now">Click here to start tracking this routine now.</a><br />
				The weight values below are not adjusted according to the routine\'s specifications.';
			$this->session->set_flashdata('allow_start_r_now', 'allow'); //UNSURE flashdata to prevent direct access to method?
			return $data;
		}
	}
	
	public function get_day_type($user_id, $selected_routine)
	{
		//TODO this should be integrated into DB
		// i feel that this can be done more efficiently
		// get the routine_tracking_id based on user_id and selected_routine (todo: selected_routine system)
		// note that $user_id is manually set in controller->routine if no user is logged in
		//$selected_routine = $this->get_selected_routine($user_id); //note that if this is used in place (below) the queries get mixed up and it breaks
		//echo 'user id is: ' . $user_id . ' that';
		// SELECT routine_tracking_id FROM yaowt_routine_tracking WHERE user_id = $user_id AND routine_id = $selected_routine 
		$this->db->select('routine_tracking_id');
		$this->db->from('yaowt_routine_tracking');
		$this->db->where('user_id', $user_id);
		$this->db->where('yaowt_routine_tracking.routine_id', $selected_routine);
		
		$query = $this->db->get();
		
		$routine_tracking_id = $query->result_array();
		//echo $user_id;
		if (isset($routine_tracking_id[0]['routine_tracking_id'])) { //TODO im here but i was curious why this function is called twice by routine controller
			//echo 'result is true <br />';
			//print_r($routine_tracking_id);
			//echo $routine_tracking_id[0]['routine_tracking_id'];
			$routine_tracking_id = $routine_tracking_id[0]['routine_tracking_id'];
		} else {
			//echo 'Error! model: routine: no example routine set or no example user set!';
			// TODO echo '-this is an example routine-SHOW EXAMPLE ROUTINE HERE--or make sure that the example routine is named accordingly in the db'; 
			$routine_tracking_id = 1; //fill with example tracking id
			
			//print_r($routine_tracking_id);
		}
		
		
		$result = $query->result();
		
		$this->db->select('starting_date');
		//echo $routine_tracking_id;
		$query = $this->db->get_where('yaowt_routine_tracking', array('routine_tracking_id' => $routine_tracking_id));
		
		$result = $query->result_array();
		
		// check if a starting_date is set, if not, ask the user a starting_date, TODO also supply cancel/go back link
		// TODO think about what should be done if the starting_date is set in the future
		// TODO this function should not check if a starting_date was set, move to its own function..
		if (empty($result[0]['starting_date'])) {
			//echo 'result was empty';
			//$data['messages'] = array();
			//array_push($data['messages'], "WWWWWWWTWATATWTWATAT");
			// message "Would you like to start this routine now? YES/NO, return to overview" // TODO 3rd option: View routine (in routine editor or something)
			// YES sets start_date to now
			// NO returns to overview page 
		} else {
			//print_r($result);
			$starting_date_str = $result[0]['starting_date'];
			
			$format = 'Y-m-d H:i:s';
			$starting_date = date_create_from_format($format, $starting_date_str);
					
			$current_date_str = date($format); //this is weird
			$current_date = date_create_from_format($format, $current_date_str);
			//echo empty($starting_date)?'empty':'not empty';
			$interval = $starting_date->diff($current_date);
			
			// using hours instead of days to allow more flexibility in the future
			$interval_hours = $interval->h;
			$interval_hours += ($interval->d*24) % 168; //based on weekly cycle
			
			
			if ($interval_hours < 0){
				return "Error: interval less than zero";
			}
			
			// set up modulus: heavy is between 0 and 48, medium 
			if ($interval_hours < 48) { // note that int casting is not required
				return "Heavy day";
			}
			elseif ($interval_hours >= 48 && $interval_hours < 96) {
				return "Medium day";
			}
			elseif ($interval_hours >= 96) {
				return "Light day";
			}
			else {
				return "Error: modulus failed at interval conditional";
			}
		}
	}


	public function get_exercise_data($user_id, $selected_routine = 1)
	{
		//required:
		//default:
		$this->db->select('tracking_id, yaowt_tracking.routine_composition_id, yaowt_exercises.exercise_name, current_weight, start_weight, yaowt_tracking.user_notes');
		$this->db->from('yaowt_tracking');
		$this->db->join('yaowt_routine_composition', 'yaowt_tracking.routine_composition_id = yaowt_routine_composition.routine_composition_id');
		$this->db->join('yaowt_exercises', 'yaowt_routine_composition.exercise_id = yaowt_exercises.exercise_id');
		//$this->db->join('yaowt_routine_composition', 'yaowt_tracking.exercise_id = yaowt_routine_composition.exercise_id');
		$this->db->where('yaowt_routine_composition.routine_id', $selected_routine);
		$this->db->where('yaowt_tracking.user_id', $user_id);
		$query = $this->db->get();
		//return $this->db->get();
			
		$result = $query->result_array();
		return $result;
	}
	
	//get exercise data for ONE split takes user_id and selected split
	public function get_split_exercise_data($user_id, $selected_split)
	{
		
		$this->db->select('yaowt_tracking.tracking_id, yaowt_tracking.split_composition_id, yaowt_exercises.exercise_name, 
		yaowt_tracking.current_weight, yaowt_tracking.start_weight, yaowt_tracking.user_reps, yaowt_tracking.user_sets, 
		yaowt_split_exercise_details.sets, yaowt_split_exercise_details.reps, yaowt_split_exercise_details.notes, yaowt_tracking.user_notes');
		$this->db->from('yaowt_tracking');
		// $this->db->join('yaowt_split_routine_composition', 'yaowt_tracking.split_routine_composition_id = yaowt_split_routine_composition.split_routine_composition_id');
		//split routine id AND split id
		$this->db->join('yaowt_split_composition', 'yaowt_tracking.split_composition_id  = yaowt_split_composition.split_composition_id');
		$this->db->join('yaowt_split_exercise_details', 'yaowt_split_composition.split_composition_id =  yaowt_split_exercise_details.split_composition_id');
		$this->db->join('yaowt_exercises', 'yaowt_split_composition.exercise_id = yaowt_exercises.exercise_id');
		//$this->db->join('yaowt_routine_composition', 'yaowt_tracking.exercise_id = yaowt_routine_composition.exercise_id');
		$this->db->where('yaowt_split_composition.split_id', $selected_split);
		$this->db->where('yaowt_tracking.user_id', $user_id);
		// $query = $this->db->get();
		// echo $this->db->last_query();
		// die;
		$result = $this->db->get()->result_array();
		//return $this->db->get();
		// $result = $query->result_array();
		return $result;
	}
	
	public function get_routine_start_date($user_id, $selected_routine)
	{
		$this->db->from('yaowt_routine_tracking');
		$this->db->where('user_id', $user_id);
		$this->db->where('routine_id', $selected_routine);
		//print_r($this->db->get()->result());
		$start_date = $this->db->get()->row()->starting_date;
		return $start_date;
	}
	
	public function get_split_routine_start_date($user_id, $selected_split_routine)
	{
		$this->db->from('yaowt_split_routine_tracking');
		$this->db->where('user_id', $user_id);
		$this->db->where('split_routine_id', $selected_split_routine);
		//print_r($this->db->get()->result());
		$start_date = $this->db->get()->row()->starting_date;
		return $start_date;
	}
	
	public function get_split_name($options = array())
	{
		//required:
		if (_required(array('split_id'), $options) == FALSE) {
			return FALSE;
		}
		$this->db->select('split_name');
		$this->db->from('yaowt_splits');
		$this->db->where('split_id', $options['split_id']);
		return $this->db->get()->row()->split_name;
	}
	
	public function get_split_number($options = array())
	{
		if (_required(array('split_id'), $options) == FALSE) {
			return FALSE;
		}
		$this->db->select('split_number');
		$this->db->from('yaowt_splits');
		$this->db->where('split_id', $options['split_id']);
		return $this->db->get()->row()->split_number;
	}
	
	public function get_split_info($options = array())
	{
		if (_required(array('split_id'), $options) == FALSE) {
			return FALSE;
		}
		$this->db->select('split_info');
		$this->db->from('yaowt_splits');
		$this->db->where('split_id', $options['split_id']);
		return $this->db->get()->row()->split_info;
	}
	
	public function get_split_exercise_details($options = array())
	{
		if (_required(array('split_composition_id'), $options) == FALSE) {
			return FALSE;
		}
		$this->db->select('sets, reps, notes');
		$this->db->from('yaowt_split_exercise_details');
		$this->db->where('split_composition_id', $options['split_composition_id']);
		return $this->db->get()->row_array();
	}
	
	public function get_exercise_instructions($options = array())
	{
		if (_required(array('exercise_id'), $options) == FALSE) {
			return FALSE;
		}
		$this->db->select('exercise_instructions');
		$this->db->from('yaowt_exercises');
		$this->db->where('exercise_id', $options['exercise_id']);
		return $this->db->get()->row()->exercise_instructions;
	}
	
	// if current_weight is not set, populate with start_weight .. why default 1?
	public function populate_current($tracking_id) //$user_id, $selected_routine = 1
	{
		// for each tracking_id where user_id = $user_id update current_weight == start_weight
		// for currently selected routine, and current user, get all start weight
		// SELECT start_weight FROM yaowt_tracking JOIN yaowt_tracking.exercise_id ON yaowt_exercises.exercise_id WHERE routine_id =$selected_routine AND user_id = $user_id 
		$this->db->select('start_weight'); //$this->db->select('tracking_id, start_weight');
		$this->db->from('yaowt_tracking');
		//$this->db->join('yaowt_exercises', 'yaowt_tracking.exercise_id = yaowt_exercises.exercise_id');
		// $this->db->join('yaowt_routine_composition', 'yaowt_tracking.routine_composition_id = yaowt_routine_composition.routine_composition_id');
		// $this->db->where('routine_id', $selected_routine);
		// $this->db->where('user_id', $user_id);
		$this->db->where('tracking_id', $tracking_id);
		$result2 = $this->db->get()->row_array();
		
		// $result2 = $query->result_array();
		// store those start_weight in a two-dimensional array
		// then use update_batch() to update current_weight with that array

		// $data = array();
		// for ($i=0; $i < count($result2); $i++) { 
			// $data[$i] = array( 
				// 'tracking_id' => $result2[$i]['tracking_id'],
				// 'current_weight' => $result2[$i]['start_weight']
			// );
		// }
		$data = array(
			'current_weight' => $result2['start_weight']
		);
		$this->db->where('tracking_id', $tracking_id);
		$this->db->update('yaowt_tracking', $data);
		// $this->db->update_batch('yaowt_tracking', $data , 'tracking_id');
	}

	///XXX DRY
	public function populate_current_split($tracking_id)
	{
		// for each tracking_id where user_id = $user_id update current_weight == start_weight
		// for currently selected routine, and current user, get all start weight 
		$this->db->select('start_weight');
		$this->db->from('yaowt_tracking');
		
		$this->db->where('tracking_id', $tracking_id);
		$result2 = $this->db->get()->row_array();
		// store those start_weight in a two-dimensional array
		// then use update_batch() to update current_weight with that array
		
		$data = array(
			'current_weight' => $result2['start_weight']
		);
		
		$this->db->where('tracking_id', $tracking_id); //AR caching for one line not really necessary me thinks
		$this->db->update('yaowt_tracking', $data);	
		
	}
	
	public function calc_weight($user_id, $result, $selected_routine, $day_type)
	{
		//TODO this should be integrated into DB
		// set exercise weight according to day_type and return it
		//$day_type = $this->get_day_type($user_id, $selected_routine); //XXX does passing as argument work?
		// note that the comparisons are case sensitive
		if ($day_type == 'Heavy day') {
			//echo 'test heavy';
			//this does nothing but remove trailing zeroes (decimals) in the view
			foreach ($result as &$exercise) {
				$exercise['current_weight'] *= 1.0;
			}
			return $result;
		}
		elseif ($day_type == "Medium day") {
			//echo 'test medium';
			foreach ($result as &$exercise) {
				$exercise['current_weight'] *= 0.9;
				
			}
			return $result;
		}
		elseif ($day_type == "Light day") {
			//echo 'test light';
			foreach ($result as &$exercise) {	//learning experience: & to modify
				//print_r($exercise);
				//$exercise['exercise_name'] = "test";
				$exercise['current_weight'] *= 0.8;
				//print_r($exercise);	
			}
			return $result;
		}
		else {
			//echo "Error model:routine: no matching day type found, showing default (heavy) day";
			return $result;
		}
	}

	//get the exercise name and current weight (and start_weight) per exercise for the current user and the selected routine
	//takes $user_id, $selected_routine, $day_type
	public function get_exercises($user_id, $selected_routine, $day_type) ///XXX change me (add option~split)
	{
		//$selected_routine = $this->get_selected_routine($user_id);				
		// get rows from y_tracking, matching user and selected routine
		$result = $this->get_exercise_data($user_id, $selected_routine); ///XXX change me(add option~split)
		
		//$selected_routine = 1; //example routine todo: change how this is set
		//$current_user = $user_id; //note that this is set to an example user if no user is logged in

		// OLD: check if the user has filled fields for the selected routine, if not, display Example routine
		// new: if the user is viewing the example routine, populate it with example weight data
		//$result = array_filter($result);
		//FIXME too much logic
		//check if 'current_weight' is set, if not, populate it using start_weight
		foreach ($result as &$exercise) {
			if (!isset($exercise['current_weight']) && ($exercise['start_weight'] > 0)) {
				//echo 'current weight is NOT set';
				// populate current_weight fields with start_weight
				// $this->populate_current($user_id, $selected_routine); //this needs $exercise['current_weight']'s identifier, should be in $exercise.. ['tracking_id']
				$this->populate_current($exercise['tracking_id']);
				// I think this function should be split into four parts:
				// get_exercise_data(name and weight), calculate_weight(for a set day type), populate_current(update current with start weight), get_exercises (calls the first three)
				
				// update result with a new query
				$result = $this->get_exercise_data($user_id, $selected_routine);
			}
		}	
		$result = $this->calc_weight($user_id, $result, $selected_routine, $day_type);
		return $result;
	}

	public function get_exercises_split($user_id, $selected_split) ///XXX change me (add option~split)
	{					
		$result = $this->get_split_exercise_data($user_id, $selected_split); ///XXX change me(add option~split)
		// new: if the user is viewing the example routine, populate it with example weight data 
		//FIXME too much logic
		//check if 'current_weight' is set, if not, populate it using start_weight
		
		foreach ($result as &$exercise) {
			if (!isset($exercise['current_weight']) && ($exercise['start_weight'] > 0)) {
				//echo 'current weight is NOT set';
				// populate current_weight fields with start_weight
				$this->populate_current_split($exercise['tracking_id']);
				
				// I think this function should be split into four parts:
				// get_exercise_data(name and weight), calculate_weight(for a set day type), populate_current(update current with start weight), get_exercises (calls the first three)
				
				// update result with a new query
				$result = $this->get_split_exercise_data($user_id, $selected_split);
				
			}
		}
		return $result;
	}

	public function get_all_exercises()
	{
		//SELECT * FROM yaowt_exercises
		$result = $this->db->get('yaowt_exercises')->result_array();
		return $result;
	}

	public function get_routine_name($user_id, $selected_routine = 1)
	{
		
		//$selected_routine = $this->get_selected_routine($user_id);
		
		$this->db->select('routine_name');
		$this->db->from('yaowt_routines');
		$this->db->where('routine_id', $selected_routine);
		$query = $this->db->get();
		$result = $query->row_array();
		return $result['routine_name'];	
	}
	
	public function get_split_routine_name($user_id, $selected_routine)
	{
		$this->db->select('routine_name');
		$this->db->from('yaowt_split_routines');
		$this->db->where('split_routine_id', $selected_routine);
		$result = $this->db->get()->row_array();
		// echo $selected_routine;
		// print_r($result);
		// die;
		return $result['routine_name'];
	}
	
	public function get_last_log_time($options = array())
	{
		// file_put_contents('testfile.txt', 'made it inside routine model');
		if (_required(array('user_id'), $options) == FALSE) {
			return FALSE;
		}
		//AR caching doesnt work the way i want it... getting ambiguous user_id etc
		
		$this->db->select('tracking_timestamp');
		$this->db->from('yaowt_tracking_history');
		$this->db->join('yaowt_tracking', 'yaowt_tracking_history.tracking_id = yaowt_tracking.tracking_id');
		$this->db->order_by('tracking_history_id', 'DESC');
		$this->db->limit('1');
		$this->db->where('yaowt_tracking.user_id', $options['user_id']);
		
		
		if ($this->db->count_all_results() >= 1) {
			$this->db->select('tracking_timestamp');
			$this->db->from('yaowt_tracking_history');
			$this->db->join('yaowt_tracking', 'yaowt_tracking_history.tracking_id = yaowt_tracking.tracking_id');
			$this->db->order_by('tracking_history_id', 'DESC');
			$this->db->limit('1');
			$this->db->where('yaowt_tracking.user_id', $options['user_id']);
			$result = $this->db->get()->row()->tracking_timestamp;
			return $result;
		} else {
			return '0';
		}
	}
	
	//FIXME the function below belongs in a helper or something
	//(this function previously required $type as argument)
	//check whether the input routine_id is the selected routine in the db; if so return true
	public function check_selected($user_id, $routine_id, $is_split_routine)
	{
		
		//SELECT routine_id FROM yaowt_selected_routine WHERE user_id = $user_id
		// $this->db->select('routine_id');
		// $this->db->from('yaowt_selected_routine');
		// $this->db->where('user_id', $user_id);
		// $query = $this->db->get();	
		// $result = $query->result_array();
		if ($is_split_routine == FALSE) {
			// if ($routine_id == $this->get_selected_routine($user_id)) { // XXX is there a prettier way to do this? ternary..
				// return TRUE;
			return ($routine_id == $this->get_selected_routine(array('user_id' => $user_id)))?TRUE:NULL;
			// echo 'test';
			// }
		} else {
			return ($routine_id == $this->get_selected_routine(array('user_id' => $user_id, 'type' => 'split')))?TRUE:NULL;
		}
		
		
		
	}
	
	//returns subbed routine_id and routine_name for $user_id
	public function get_subbed_routines($user_id)
	{
		// SELECT routine_id, routine_name FROM yaowt_routine_tracking JOIN yaowt_routines ON yaowt_routine_tracking.routine_id = yaowt_routines.routine_id WHERE user_id = $user_id
		$this->db->select('yaowt_routines.routine_id, routine_name');
		$this->db->from('yaowt_routine_tracking');
		$this->db->join('yaowt_routines', 'yaowt_routine_tracking.routine_id = yaowt_routines.routine_id');
		$this->db->where('user_id', $user_id);
		$query = $this->db->get();
		$result = $query->result_array();
		return $result;
	}
	
	public function get_subbed_split_routines($user_id)
	{
		$this->db->select('yaowt_split_routines.split_routine_id, routine_name');
		$this->db->from('yaowt_split_routine_tracking');
		$this->db->join('yaowt_split_routines', 'yaowt_split_routine_tracking.split_routine_id = yaowt_split_routines.split_routine_id');
		$this->db->where('user_id', $user_id);
		$query = $this->db->get();
		$result = $query->result_array();
		return $result;
	}
	
	public function get_available_routines($routine_ids)
	{
		//get available routines
		$this->db->from('yaowt_routines');
		$this->db->where_not_in('routine_id', $routine_ids);
		$available_routines = $this->db->get()->result_array();
		return $available_routines;
	}
	
	public function get_all_routines()
	{
		$this->db->from('yaowt_routines');
		return $this->db->get()->result_array();
	}
	
	public function get_available_split_routines($split_routine_ids)
	{
		$this->db->from('yaowt_split_routines');
		$this->db->where_not_in('split_routine_id', $split_routine_ids);
		$available_split_routines = $this->db->get()->result_array();
		return $available_split_routines;
	}
	
	public function get_all_split_routines()
	{
		$this->db->from('yaowt_split_routines');
		return $this->db->get()->result_array();
	}
	
	//FIXME this function requires $type as input for get_Selected_routine
	public function unsub_routine($user_id, $form_selected_routine)
	{
		//if $form_selected_routine matches $selected_routine, remove routine selection data(WHERE user_id AND routine_id FROM y_selected_routine
		//this seems rather superfluous
		$selected_routine = $this->get_selected_routine(array('user_id' => $user_id));
		if ($form_selected_routine == $selected_routine) {
			$this->db->where('user_id', $user_id);
			$this->db->where('routine_id', $form_selected_routine);
			$this->db->delete('yaowt_selected_routine');
		}
		// DELETE FROM yaowt_routine_tracking WHERE user_id = $user_id AND routine_id = $form_selected_routine
		$this->db->where('user_id', $user_id);
		$this->db->where('routine_id', $form_selected_routine);
		$this->db->delete('yaowt_routine_tracking');
	}
	
	public function unsub_split_routine($user_id, $form_selected_routine)
	{
		// DELETE FROM yaowt_routine_tracking WHERE user_id = $user_id AND routine_id = $form_selected_routine
		$this->db->where('user_id', $user_id);
		$this->db->where('split_routine_id', $form_selected_routine);
		$this->db->delete('yaowt_split_routine_tracking');
	}
	
	public function sub_routine($user_id, $chosen_routine)
	{
		$data = array(
			'user_id' => $user_id,
			'routine_id' => $chosen_routine
		);
		$this->db->insert('yaowt_routine_tracking', $data);
	}

	public function sub_split($user_id, $chosen_routine)
	{
		$data = array(
			'user_id' => $user_id,
			'split_routine_id' => $chosen_routine
		);
		$this->db->insert('yaowt_split_routine_tracking', $data);
	}

	
	//insert new rows into the tracking table for current user and selected routine
	public function add_missing_tracking_row($options = array()) //rename this to 'add_tracking_row' or something TODO
	{
		//required:
		if (_required(array('user_id', 'routine_compo_id', 'type'), $options) == FALSE) {
			return FALSE;
		}
		
		$data = array(
			'user_id' => $options['user_id'],
			split_or_routine($options['type']) . '_composition_id' => $options['routine_compo_id']
		);
		
		$sql = $this->db->insert_string('yaowt_tracking', $data) . ' ON DUPLICATE KEY UPDATE user_id = "' . $options['user_id']. '"';
			//$this->db->insert('yaowt_selected_routine', $data);
			$this->db->query($sql);
		
		// $this->db->insert('yaowt_tracking', $data);
		
		// //echo 'populating empty';
		// $this->db->from('yaowt_routine_composition');
		// $this->db->where('routine_id', $selected_routine);
		// $exercises = $this->db->get()->result_array();
// 		
		// //$exercises = "an array of exercise_id?"; //or just the routine composition table!
// 		
		// //FIXME too much logic
		// foreach ($exercises as $key => $value) {
			// if ($value) {
// 				
			// }
			// //(multiple)SELECT $routine_composition_id FROM yaowt_routine_composition WHERE routine_id = $selected_routine AND exercise_id = $exercise['exercise_id']
			// $routine_composition_id = $exercises[$key]['routine_composition_id'];
			// // $start_weight = 0; ///XXX commented these 2 out
			// // $current_weight = 0;  
			// //INSERT INTO yaowt_tracking (user_id, routine_composition_id, start_weight, current_weight) VALUES ($user_id, $routine_composition_id, $start_weight, $current_weight)
			// $data = array(
				// 'user_id' => $user_id,
				// 'routine_composition_id' => $routine_composition_id,
				// // 'start_weight' => $start_weight,  //XXX commented these 2 out
				// // 'current_weight' => $current_weight
			// );
			// //do not overwrite present rows
			// $insert_query = $this->db->insert_string('yaowt_tracking', $data); //method for using IGNORE with CI's AR
			// $insert_query = str_replace('INSERT INTO', 'INSERT IGNORE INTO', $insert_query);
			// //$this->db->insert('yaowt_tracking', $data);
			// $this->db->query($insert_query);
		// }
		// redirect(base_url('routine')); //bad!
	}

	public function get_routine_composition($selected_routine)
	{
		//SELECT * FROM yaowt_routine_composition WHERE routine_id = $selected_routine
		$this->db->from('yaowt_routine_composition');
		//$this->db->where('user_id', $user_id);
		$this->db->where('routine_id', $selected_routine);
		$result = $this->db->get()->result_array();
		//print_r($result);
		return $result;
	}
	
	//get all split_composition_ids for the split the user has selected
	public function get_split_composition_ids($selected_split)
	{
		$this->db->select('split_composition_id');
		$this->db->from('yaowt_split_composition');
		$this->db->where('split_id', $selected_split);
		$result = $this->db->get()->result_array();
		return $result;
	}

	//i guess this gets them for the entire routine (all splits)
	public function get_split_routine_composition_ids($selected_split_routine)
		{
			$this->db->select('split_composition_id');
			$this->db->from('yaowt_split_composition');
			$this->db->join('yaowt_split_routine_composition', 'yaowt_split_composition.split_id = yaowt_split_routine_composition.split_id');
			$this->db->where('split_routine_id', $selected_split_routine);
			$result = $this->db->get()->result_array();
			return $result;
		}
	
	// updates one row with new weight data in the tracking table
	public function update_track_row($user_id, $tracking_id, $new_start_weight, $new_current_weight, $user_notes, $new_user_reps, $new_user_sets)
	{
		//FIXME too much logic
		if (!($new_start_weight == NULL)) {
			$this->db->set('start_weight', $new_start_weight);
		}
		if (!($new_current_weight == NULL)) {
			$this->db->set('current_weight', $new_current_weight);
		}
		if (!($user_notes == NULL)) {
			$this->db->set('user_notes', $user_notes);
		}
		if (!($new_user_reps == NULL)) {
			$this->db->set('user_reps', $new_user_reps);
		}else {
			$this->db->set('user_reps', NULL);
		}
		if (!($new_user_sets == NULL)) {
			$this->db->set('user_sets', $new_user_sets);
		}else {
			$this->db->set('user_sets', NULL);
		}
		// below else if no longer required, since a legal update will always happen (null or not null)
		// else if (($new_start_weight == NULL) && ($new_current_weight == NULL) && ($user_notes == NULL) && ($new_user_reps == NULL) && ($new_user_sets == NULL)){
			// return FALSE;
		// }
		
		$this->db->where('user_id', $user_id); //superflous because of tracking id
		$this->db->where('tracking_id', $tracking_id);
		$this->db->update('yaowt_tracking');
	}


	public function insert_split_routine($routine_name, $routine_info)
	{
		$data = array(
			'routine_name' => $routine_name,
			'routine_info' => $routine_info
			//TODO FUTURE see controller
		);
		$this->db->insert('yaowt_split_routines', $data);
		//save id for split_routine_composition
		$split_routine_id = $this->db->insert_id();
		return $split_routine_id;
	}
	
	public function insert_splits($split_number, $split_name, $split_info)
	{
		$data = array(
			'split_number' => $split_number,
			'split_name' => $split_name,
			'split_info' => $split_info
		);
		$this->db->insert('yaowt_splits', $data);
		
		$split_id = $this->db->insert_id();
		return $split_id;
	}
	
	public function insert_split_composition($split_id, $exercise_id)
	{
		$data = array(
			'split_id' => $split_id,
			'exercise_id' => $exercise_id
		);
		$this->db->insert('yaowt_split_composition', $data);
	}

	public function insert_split_exercise_details($split_composition_id, $sets, $reps, $notes) //TODO use options array
	{

		$data = array(
			'split_composition_id' => $split_composition_id,
			'sets' => $sets,
			'reps' => $reps,
			'notes' => $notes
		);
		$this->db->insert('yaowt_split_exercise_details', $data);
		return $this->db->affected_rows();
	}

	public function insert_split_routine_composition($split_routine_id, $split_id)
	{
		$data = array(
			'split_routine_id' => $split_routine_id,
			'split_id' => $split_id
		);
		$this->db->insert('yaowt_split_routine_composition', $data);
	}
	
	//returns 1 on success and 0 on failure
	public function add_new_exercise($options = array())
	{
		if (_required(array('name', 'instructions'), $options) == FALSE) {
			return FALSE;
		}
		$data = array();
		$data['exercise_name'] = isset($options['name'])?$options['name']:NULL; 
		$data['exercise_instructions'] = isset($options['instructions'])?$options['instructions']:NULL;
		
		$insert_query = $this->db->insert_string('yaowt_exercises', $data);
		$insert_query = str_replace('INSERT INTO', 'INSERT IGNORE INTO', $insert_query);
		$this->db->query($insert_query);		
		return $this->db->affected_rows(); //return (result/query) success or failure

	}
	
}


/* End of file routine_model.php */
/* Location: .application/controllers/routine_model.php */