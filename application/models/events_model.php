<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Events_model extends CI_Model {
	function __construct() {
		parent::__construct();
		
	}
	public function get_condition_id($options = array())
	{
		if (_required(array('user_id', 'split_routine_id'), $options) == FALSE) {
			return FALSE;
		}
		
		$this->db->select('condition_id');
		$this->db->from('yaowt_split_routine_tracking');
		$this->db->where('user_id', $options['user_id']);
		$this->db->where('split_routine_id', $options['split_routine_id']);
		return $this->db->get()->row()->condition_id;
	}
	
	public function set_condition($options = array())
	{
		if (_required(array('condition_id', 'user_id', 'split_routine_id'), $options) == FALSE) {
			return FALSE;
		}
		
		$this->db->where('user_id', $options['user_id']);
		$this->db->where('split_routine_id', $options['split_routine_id']);
		$data = array(
			'condition_id' => $options['condition_id']
		);
		$this->db->update('yaowt_split_routine_tracking', $data);
		return $this->db->affected_rows();
	}
	
	public function get_success_id($options = array())
	{
		if (_required(array('split_routine_id'), $options) == FALSE) {
			return FALSE;
		}
		$this->db->select('success_event_id');
		$this->db->from('yaowt_split_routines');
		$this->db->where('split_routine_id', $options['split_routine_id']);
		return $this->db->get()->row()->success_event_id;
	}
	
	public function get_fail_id($options = array())
	{
		if (_required(array('split_routine_id'), $options) == FALSE) {
			return FALSE;
		}
		$this->db->select('fail_event_id');
		$this->db->from('yaowt_split_routines');
		$this->db->where('split_routine_id', $options['split_routine_id']);
		return $this->db->get()->row()->fail_event_id;
	}
	
	public function get_split_routine_exercise_data($options = array())
	{
		if (_required(array('selected_split_routine'), $options) == FALSE) {
			return FALSE;
		}
		$this->db->select('tracking_id, current_weight');
		$this->db->from('yaowt_tracking');
		$this->db->join('yaowt_split_composition', 'yaowt_tracking.split_composition_id = yaowt_split_composition.split_composition_id'); //split_composition_id
		$this->db->join('yaowt_split_routine_composition', 'yaowt_split_composition.split_id = yaowt_split_routine_composition.split_id');
		$this->db->where('yaowt_split_routine_composition.split_routine_id', $options['selected_split_routine']);
		return $this->db->get()->result_array();
	}
	
}

/* End of file events_model.php */
/* Location: .application/controllers/events_model.php */