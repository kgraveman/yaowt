<div class="contact_form submain">
	<h2>Contact form:</h2>


	<?php echo form_open('contact'); ?>
	<div>
		<label for="sender">Your name:</label>
		<br />
		<input type="text" name="sender" id="sender" size="30" style="width: 90%;" required="required" pattern=".{3,50}" title="3 to 50 characters" autofocus="" class="<?php echo(form_error('sender') ? 'textInputError' : ''); ?>" value="<?php echo set_value('sender'); ?>"/>
	</div>

	<div>
		<label for="email_r">Your email address:</label>
		<br />
		<input type="email" name="email_r" id="email_r" size="30" style="width: 90%;" required="required" pattern=".{5,200}" title="5 to 200 characters" class="<?php echo(form_error('email_r') ? 'textInputError' : ''); ?>" value="<?php echo set_value('email_r'); ?>"/>
	</div>

	<div>
		<label for="message_body">Your message:</label>
		<br />
		<textarea rows="10" cols="60" style="width: 90%;" name="message_body" id="message_body" required="required"  maxlength="5000" class="<?php echo(form_error('message_body') ? 'textInputError' : ''); ?>"><?php echo set_value('message_body'); ?></textarea>
	</div>
	<div class="magic">
		<input type="text" name="email" />
		<input type="hidden" name="comment_h" id="comment_h" value=""/>
	</div>
	
		<div>
	
	<?php
	echo 'Please complete the following CAPTCHA: <br />';
	echo $cap['image'];
	?>
	
</div>
	
	<div>
		<label for="captcha">Enter CAPTCHA:</label>
		<input type="text" name="captcha" id="captcha"/>
	</div>
	
	<input type="submit" value="Submit" />
	</form>
</div>

