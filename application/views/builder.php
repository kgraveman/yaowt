<div id="builder" class="submain">
	<p>
		This is the builder, here you can create your own routines. Currently only creating split routines is supported. Condition based events will be added soon. Preview how it works by playing with the buttons shown on the first and last days of "A Simple Beginner's Routine". 
	</p>
	
	<a href="<?php echo base_url('new_exercise'); ?>" target="_blank">Add a new exercise</a> <span>(opens a new window)</span>
	<br />
	
	<?php echo form_open('builder'); ?>
	<fieldset class="aligned_form">
		<legend><h2>Create routine</h2></legend>
		<label for="r_name">Routine name:</label>
		<input type="text" name="r_name" id="r_name" value="<?php echo set_value('r_name'); ?>" /> <?php //FIXME others need label too ?>
		<br />
		<label for="r_info">Routine info:</label>
		<input type="text" name="r_info" id="r_info" value="<?php echo set_value('r_info'); ?>" />
		<br />
		
		<hr />
		
		<ul class="d_days">
				
			<li class="day"> Split day: <span class="day_num"><strong>Day 1</strong></span>
				<br />
				<label><span class="input_label">Day name:</span>
				<input type="text" name="d_name[]" id="d_name[]" value="<?php echo set_value('d_name[]'); ?>" /></label>
				<br />
				<label><span class="input_label">Day info:</span>
				<input type="text" name="d_info[]" id="d_info[]" value="<?php echo set_value('d_info[]'); ?>" /></label>
				
				<br />
				<div class="d_exercises">
					<div class="exercise">
						<div>
							<label><span class="input_label">Exercise:</span>
							<select id="first_ex" name="exer_sel_1[]">
		<?php foreach ($ex_names as $key => $value) {
									echo "\t\t\t\t\t\t<option value='" . $ids[$key] . "'>" . $value . "</option>\n";
								}; ?>
							</select></label>
							<input type="submit" class="view_instr" title="Opens a popup"  formaction="builder/instructions" formtarget="_blank" value="View exercise instructions" />
						</div>
						<div>
							<label><span class="input_label">reps:</span><input type="number" name="reps_1[]" id="reps_1[]" value="<?php echo set_value('reps_1[]'); ?>" /><br /></label>
							<label><span class="input_label">sets:</span><input type="number" name="sets_1[]" id="sets_1[]" value="<?php echo set_value('sets_1[]'); ?>" /><br /></label>
							<label><span class="input_label">notes:</span><textarea rows="1" name="notes_1[]" id="notes_1[]" placeholder="Notes"><?php echo set_value('notes_1[]'); ?></textarea></label>
						</div>
					</div>
				</div>
				<h4>
					<a href="#" class="add_ex">Add another exercise</a>
				</h4> 
			</li>
		</ul>
		
		<h3><a href="#" class="add_day">Add another day</a></h3>
		
		<!-- <p class="test"></p> -->
		<input type="submit" value="Submit" onclick="return confirm('Submit new routine to database?')" />
	</fieldset>
	</form>
	
</div>