<?php if (!defined('BASEPATH'))	exit('No direct script access allowed'); ?>

<script>

	$(function() {
    	$.ajaxSetup({
        	data: {
            	<?php echo $this->config->item('csrf_token_name'); ?>: $.cookie('<?php echo $this->config->item('csrf_cookie_name'); ?>')
			}
		});
	});


	$(function(){
		$(document).on('click', '.view_instr', function(e) {
			// var exercise_id = $(this).siblings().children().children(":selected").attr("value");
			// var exercise_id = $(this).siblings().closest('select').val();
			var exercise_id = $(this).siblings('label').children('select').val();
			$.ajax({	url: '/yaowt/ajax',
						data: {action: 'get_instructions', data: exercise_id},
						type: 'POST',
						success: function(output){
							alert(output);
						}
			});
			e.preventDefault();
		});
	});

	$(function() {
		var exerDiv = $('.d_exercises');
		var i = $('.d_exercises div').length + 1; //going to have to use a pseudo selector or something for this
		//add an exercise
		$(document).on('click', '.add_ex', function(e) {
			var j = $(this).closest('li').index() + 1;
			
			$(this).parent().prev().append('<div class="exercise"><div><label for="exer_sel_' + j + '[]">Exercise:</label><select name="exer_sel_' + j + '[]" id="exer_sel_' + j + '[]">\
				<?php foreach ($ex_names as $key => $value) {
					echo '<option value="' . $ids[$key] . '">' . $value . '</option>';
				}; ?></select>\
			<input type="submit" class="view_instr" title="Opens a popup"  formaction="builder/instructions" formtarget="_blank" value="View exercise instructions" />\
			</div>\
			<div>\
				<label><span class="input_label">reps:</span><input type="number" name="reps_' + j + '[]" id="reps_' + j + '[]"/></label>\
				<label><span class="input_label">sets:</span><input type="number" name="sets_' + j + '[]" id="sets_' + j + '[]"/></label>\
				<br /><label><span class="input_label">notes:</span><textarea rows="1" name="notes_' + j + '[]" id="notes_' + j + '[]"></textarea></label><br />\
			 <a href="#" class="rem_ex">Remove exercise</a></div></div>');
			i++;
			e.preventDefault();
		});

		$(document).on('click', '.rem_ex', function(e) {
			if (i > 2) {
				$(this).parents('.exercise').remove();
				i--;
			}
			e.preventDefault();
		});
	});
	
	$(function(){
		var dayDiv = $('.d_days');
		//var j = $('.day').length + 1;
		// add a day
		$('.add_day').on('click', function(e){
			var j = $('.day').length + 1;
			
			dayDiv.append('<li class="day"> Split day: <span class="day_num"><strong>Day ' + j + 
			'</strong></span> <a href="#" class="rem_day">Remove day</a>\
		<br />\
		<label><span class="input_label">Day name:</span>\
		<input type="text" name="d_name[]" /></label>\
		<br />\
		<label><span class="input_label">Day info:</span>\
		<input type="text" name="d_info[]" /></label>\
		<br />\
		<div class="d_exercises">\
			<div class="exercise"><div>\
				<label><span class="input_label">Exercise:</span>\
				<select name="exer_sel_' + j + '[]">\
					<?php foreach ($ex_names as $key => $value) {
						echo '<option value="' . $ids[$key] . '">' . $value . '</option>';
					}; ?>
				</select></label>\
			</label><input type="submit" class="view_instr" title="Opens a popup"  formaction="builder/instructions" formtarget="_blank" value="View exercise instructions" />\</div>\
			<div>\
				<label><span class="input_label">reps:</span><input type="number" name="reps_' + j + '[]" /></label>\
				<label><span class="input_label">sets:</span><input type="number" name="sets_' + j + '[]" /></label>\
				<br /><label><span class="input_label">notes:</span><textarea rows="1" name="notes_' + j + '[]"></textarea></label>\
			</div></div>\
		</div> <h4><a href="#" class="add_ex">\
		Add another exercise</a></h4> </li>');
		//j++;
		e.preventDefault();
		});
		
		$(document).on('click', '.rem_day', function(e){
			var j = $('.day').length + 1;
			// not sure if the below conditional is worth it...
			if (j > 2) {
				if ($(this).closest('.day')[0] == $(this).closest('.day').siblings().addBack().last()[0]){
					// just remove it, no remaning required
					 $(this).closest('.day').remove();
				}
				else {
					$(this).closest('.day').remove();
					// rename the day numbers
					$('.day_num').each(function(index){
						$(this).children('strong').html("Day " + (index + 1));
					});
				;}
			};
			e.preventDefault();
		});
 		
	});
	
	// $(function(){
		// $(document).on('click', '.day_num', function(){
			// //alert('test');
			// $('.day_num').html('changed!');
		// });
	// });

</script>

<?php
/* End of file builder_script.php */
/* Location: .application/controllers/builder_script.php */
