<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

// (base) url helper:
// source: http://stackoverflow.com/questions/1420881/
//UNSURE around line 20 removed : from: url = protocol + '://' + host;
?>

<?php 
//sources http://stackoverflow.com/questions/5257923/how-to-load-local-script-files-as-fallback-in-cases-where-cdn-are-blocked-unavai
//http://stackoverflow.com/a/7154317 CSRF bug -> http://jerel.co/blog/2012/03/a-simple-solution-to-codeigniter-csrf-protection-and-ajax
 ?>
<!--[if lte IE 8]><script language="javascript" type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/flot/0.8.1/excanvas.min.js"></script><![endif]-->

<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/flot/0.8.1/jquery.flot.min.js"></script>
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/flot/0.8.1/jquery.flot.time.min.js"></script>

<script type="text/javascript">
	pathArray = window.location.href.split( '/' );
	protocol = pathArray[0];
	host = pathArray[2];
	url = protocol + '//' + host;
	base_url = url + '/yaowt/';

	if (typeof $.plot === 'undefined') {
		document.write('<script type="text/javascript" src="' + base_url +  'js/jquery.flot.min.js"><\/script>');
		document.write('<script type="text/javascript" src="' + base_url +  'js/jquery.flot.time.min.js"><\/script>');
	}
	
	function get_data(virgin){
		$.ajax({
			type: "POST",
			url: base_url + 'flot_data',
			data: {action: "get_data"},
			dataType: "json"
		}).done(function(result){
			check_data(result, virgin);
		});
	}

	// check if the user has data for the selected routine, if not, get example data
	// also changes graph title
	function check_data(data, virgin){
		if(data == "empty"){
			var graph_title = "Not enough data for the selected routine";
			$.ajax({
				type: "POST",
				url: base_url + 'flot_data',
				data: {action: "get_example"},
				dataType: "json"
			
			}).done(function(result){
				make_tabs(result, virgin, graph_title);
			});
		} else{
			var graph_title = 'Workout history for the selected routine';
			make_tabs(data, virgin, graph_title)
		}
	}
		
	function make_tabs(data, virgin, graph_title){
	//give the graph a title
	$('#flot_tabs_wrap').wrap("<div id='flot_title'>" + graph_title + "</div>");
	
	//define (global) flot options
	var options = {
		 	legend: {position: "nw", backgroundColor: "null", color: "#FF0000"},
		 	grid: {clickable: true, hoverable: true},
		 	lines: {show: true},
		 	points: {show: true},
		 	xaxis: {mode: "time", timeformat: "%m/%d", timezone: "browser", minTickSize: [1, "day"]}
		 };
	
	//per exercise (data collection) make a tab
		$(data).each(function(index, value) {
	
			if (value.exercise_name != 'Rest'){
				$('#flot_tabs_wrap ul').append('<li><a href="#flot_tab_' + (index+1) + '">' + value.exercise_name + '</a></li>');
				$('#flot_tabs_wrap').append('<div class="flot_tabs" title="" id="flot_tab_' + (index+1) + '"></div>');
				
				//show the initial dataless helper tooltip
				if (virgin == true){
					$('.flot_tabs').tooltip({track: true, content: "Try hovering over points (click to hide)"});
				}
				var d1 = [];
				//for each coord, add to data feed for plot
				$(data[index].coords).each(function(index, value){
					d1.push([value.time, value.av_1rm]);
				});

		  		//plot that data feed
		  		$.plot("#flot_tab_" + (index + 1), [{ label: "1RM", data: d1}], options);

		  		//show tooltips
		  		// $("#flot_tab_" + (index + 1)).bind("plothover", function (event, pos, item){
		  		$(".flot_tabs").bind("plothover", function (event, pos, item){
		  			// $(".flot_tabs").tooltip("enable");
					if (item) {
						virgin = false;
						
						var point = 'One rep max for ' + new Date(item.datapoint[0]) + ' was: ' + item.datapoint[1];

						// $(".flot_tabs").tooltip();
						$(".flot_tabs").tooltip("option", "content", point);
						$(".flot_tabs").tooltip("option", "track", true);
						
					}
					else if (virgin == false){
					}
				});
			}
		});

		$(".flot_tabs").on("dblclick", function(){
			$(".flot_tabs").tooltip("option", {disabled: false});
		});
		
		$(".flot_tabs").on("click", function(){
			if (($(".flot_tabs").tooltip("option", "disabled")) == false){
				$(".flot_tabs").tooltip("option", {disabled: true});
			}
		});

		$(function(){
			$('#flot_tabs_wrap').tabs({
				event: "mouseover"
			});
		});
	};
	
	
	$(function() {
    	$.ajaxSetup({
        	data: {
            	<?php echo $this->config->item('csrf_token_name'); ?>: $.cookie('<?php echo $this->config->item('csrf_cookie_name'); ?>')
			}
		});
	});
	

	$(document).ready(function(){
		var virgin = true; //UNSURE if this is the way for full scope vars..
		get_data(virgin);
	});

</script>

<?php
/* End of file overview_script.php */
/* Location: .application/controllers/overview_script.php */