<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
?>

<script>
	$(function() {
    	$.ajaxSetup({
        	data: {
            	<?php echo $this->config->item('csrf_token_name'); ?>: $.cookie('<?php echo $this->config->item('csrf_cookie_name'); ?>')
			}
		});
	});

	$(document).ready(function(){
		$(document).on('click', '#success', function(){
			var confirmed = confirm("Your weight data might be automatically updated. Please confirm.");
			if (confirmed == true) {
				var success = $(this).attr("value");
				$.ajax({
					url: '/yaowt/ajax',
					data: {action: 'run_condition', data: success},
					type: 'POST',
					dataType: 'html',
					success: function(output){
						alert(output);
					}
				});
				$('.event_buttons').attr({
					disabled: 'disabled',
					title: 'Using these buttons after already using them might result in big errors for you!'
				});
			}
		});
		$(document).on('click', '#unrealized', function(){
			var confirmed = confirm("Didn\'t hit the workout target. Please confirm.");
			if (confirmed == true) {
				var unrealized = $(this).attr("value");
				$.ajax({
					url: '/yaowt/ajax',
					data: {action: 'run_condition', data: unrealized},
					type: 'POST',
					dataType: 'html',
					success: function(output){
						alert(output);
					}
				});
				$('.event_buttons').attr({
					disabled: 'disabled',
					title: 'Using these buttons after already using them might result in big errors for you!'
				});
			}
		});
		$('input').keypress(function(e){
			if (e.which == 44) {
				e.preventDefault();
				alert('Please use a period instead of a comma.');
			}
		});
		$(function() {
			$("#datepicker").datetimepicker({
				dateFormat: "yy-mm-dd",
				timeFormat: "HH:mm:ss",
				addSliderAccess: true,
				sliderAccessArgs: {touchonly: false }
			});
		});
		
		// quick change all weight values
		//get initial values
		function get_old_values() {
			var old_values = [];
			$('.current_weight').each(function(){
				old_values.push($(this).val());
			});
			return old_values;
		}
		
		$(function(){
			//define variables
			var old_values = get_old_values();
			//change values after delay, after last keyboard input 
			$('#math_value').keypress(function(){
				delayed_log(old_values);
			});
			//change values on click of a math operator
			$('.math_operator input').click(function(){
				update_inputs(old_values);
				
			}); //self and children? (add self back)
		});

		function delayed_log(old_values) {
			window.setTimeout(function(){
				update_inputs(old_values);
				}, 350);
		}

		function update_inputs(old_values){
			var math_value = $('#math_value').val();
			var radio = $('input[name=operator]:checked', '#table_form').val();
			
			$('.current_weight').each(function(key, value){
				switch (radio) {
					case ('multiply'):
						var new_value = old_values[key] * $('#math_value').val();
						$('#to_by_or_not_to_by').html("by");
						break;
					case ('divide'):
						var new_value = old_values[key] / $('#math_value').val();
						$('#to_by_or_not_to_by').html("by");
						break;
					case 'add':
						var new_value = +old_values[key] + +$('#math_value').val();
						$('#to_by_or_not_to_by').html("<span style=\"display: inline-block; width: 2ex\";></span>");
						break;
					case 'subtract':
						var new_value = old_values[key] - $('#math_value').val();
						$('#to_by_or_not_to_by').html("<span style=\"display: inline-block; width: 2ex\";></span>");
						break;
					default:
						console.log("unknown radio value");
						break;
				}
				new_value = Math.round(new_value * 100) / 100;
				$(this).val(new_value);
			});
		}
		// end quick change all weight values
		
		<?php echo (isset($dynamic_script)?$dynamic_script:''); ?>
	});
	

	
</script>



<?php
/* End of file split_routine_script.php */
/* Location: .application/controllers/split_routine_script.php */