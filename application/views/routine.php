<?php //echo isset($messages)?foreach ($$messages as $message) {
	//echo $message;
//} $messages:''; ?>

<?php if (isset($messages)) {
	foreach ($messages as $message) {
		echo '		<span class="message">' . $message . '</span>';
	}
}; ?>

<?php echo '			<h3>Routine: ' . $routine_name . '</h3>'; ?>


<?php echo form_open('routine/update_startdate'); ?>
	<span>Start date for this routine:</span>
	<input name="datetime" type="text" id="datepicker" value="<?php echo $start_date; ?>" />
	<input type="submit" value="Update start date" />
	<!-- <input type="text" name="basic_example_1" id="basic_example_1" value="" /> -->
	<!-- <input type="text" id="matchStartTime" /> -->
</form>


<?php echo '<h3>' . $day_type . '</h3>'; ?>

<?php echo '			<style type="text/css" media="screen">table td{border:1px solid black;} input{text-align: right;} ></style>';
	echo '<div style="width: 25em; margin-bottom: 2em;">' .
	form_open('routine/update_weight'); ?>

<?php foreach ($exercises as $exercise => $value) :
		echo '			<table style="background: yellow; border: 1px solid black; margin: auto; text-align: right;">
				<caption>' . $exercises[$exercise]['exercise_name'] . '</caption>
				<thead>
					<tr>
						<th style="text-align: center;">Starting weight</th>
						<th style="text-align: center;">Current weight</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>
							<input type="hidden" name="' . $exercise .  '" value=' . $exercises[$exercise]['tracking_id'] . ' />
							<input type="number" placeholder="Starting weight, please." inputmode="numeric" name="' . $exercises[$exercise]['tracking_id'] . '_S" value="' . $exercises[$exercise]['start_weight'] . '" />
						</td>
						<td><input type="number" placeholder="Current weight, please." inputmode="numeric" name="' . $exercises[$exercise]['tracking_id'] . '_C" value="' . $exercises[$exercise]['current_weight'] . '" /></td>
					</tr>
					<tr>
						<td>' . $exercises[$exercise]['start_weight'] . '</td>
						<td>' . $exercises[$exercise]['current_weight'] . '</td>
					</tr>
				</tbody>
				
			</table><br />';
	
	// echo "new line <br />";
	// print_r($exercise);
	// echo $exercises[$exercise]['weight'];
	// echo "<br />";
endforeach;
?>

		<input type="submit" value="Update weights" onclick="return confirm('Update weights and notes. Are you sure?')" />
		<input type="reset" value="Undo changes" onclick="return(confirm('Undo changes. Are you sure?'))" />
	</form>
</div>
<span>Note that current weight is calculated according to the current day type. 
	(TODO: Please use the editor for more control) </span>

<?php
//planned:
//add view current/all exercises
//next/previous day
//adjust?
//table per exercise or entire day in one table?


//DUMP:
/*{exercises}
<table>
	<caption><?php $name?></caption>
	<thead>
		<tr>
			<th>Weight</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td>{weight}</td>
		</tr>
		<tr>
			<td>{weight}</td>
		</tr>
	</tbody>
	
</table>
{/exercises}
*/

/* End of file routine.php */
/* Location: .application/controllers/routine.php */