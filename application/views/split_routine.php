<div id="split_routine" class="submain">

	<?php if (isset($messages)) {
		foreach ($messages as $message) {
			echo isset($message)?('		<span class="message">' . $message . '</span>'):'';
		}
	}; ?>
	<?php 
		$table_part_2 = '			<th style="text-align: center;">Starting weight</th>
							<th style="text-align: center;">Current weight</th>
							<th style="text-align: center;">Reps<sup onclick="alert(\'Values belonging to the routine by default are shown in blue. You can also view those values by hovering your mouse over the values. Be sure to fill these in with your own values if you want to use the graph feature.\')" 
						title="Values belonging to the routine by default are shown in blue. You can also view those values by hovering your mouse over the values. Be sure to fill these in with your own values if you want to use the graph feature.">?</sup></th>
							<th style="text-align: center;">Sets<sup onclick="alert(\'Values belonging to the routine by default are shown in blue. You can also view those values by hovering your mouse over the values Be sure to fill these in with your own values if you want to use the graph feature..\')" 
						title="Values belonging to the routine by default are shown in blue. You can also view those values by hovering your mouse over the values. Be sure to fill these in with your own values if you want to use the graph feature.">?</sup></th>
							<th class="table_notes" style="text-align: center;">Notes</th>';
				?>
	
	<?php echo '			<h3>Routine: ' . html_escape($routine_name) . '</h3>'; ?>
	<?php //<!--TODO Routine description placeholder: popup --> ?>
	
	<?php echo form_open('split_routine/update_startdate'); ?>
		<span>Start date for this routine:</span>
		<input name="datetime" type="text" id="datepicker" value="<?php echo $start_date; ?>" />
		<input type="submit" value="Update start date" onclick="return confirm('Update start date. Are you sure?')" />
	</form>
	
	
	<?php echo '<h3>Split: ' . html_escape($split_name) .' (' . $split_number . '/' . $total_splits . ')</h3>'; ?>
	<?php echo html_escape($split_info); ?>
	<?php echo '			<style type="text/css" media="screen">input{text-align: right;} ></style>';
		echo '<div>' .
		form_open('split_routine/update_weight', array('id' => 'table_form')); ?>
	
	<?php foreach ($exercises as $key => $value) :
			echo '			<table style="margin: auto; text-align: right;">
					<caption>' . html_escape($value['exercise_name']) . '</caption>
					<thead>
						<tr>' . (html_escape($value['exercise_name']) == 'Rest'?'':$table_part_2) . 
							'<th style="text-align: center;">Your notes</th>
						</tr>
					</thead>
					<tbody>
						<tr>' .
								(html_escape($value['exercise_name']) == 'Rest'?'':'<td><input class="input_number" type="number" placeholder="Input weight" inputmode="numeric" name="' . 
								$value['tracking_id'] . '_SW" value="' . $value['start_weight'] . '" />
							</td>
							<td><input class="input_number current_weight" type="number" placeholder="Input weight" inputmode="numeric" name="' . 
							$value['tracking_id'] . '_CW" value="' . $value['current_weight'] . '" /></td>
							<td><input class="input_number" type="number" placeholder="' . $value['reps'] . 
							'" inputmode="numeric" name="' . $value['tracking_id'] . '_R" value="' . $value['user_reps'] . 
							'" title="' . $value['reps'] . '" /></td>
							<td><input class="input_number" type="number" placeholder="' . $value['sets'] . 
							'" inputmode="numeric" name="' . $value['tracking_id'] . '_S" value="' . $value['user_sets'] . 
							'" title="' . $value['sets'] . '" /></td>
							<td>' . html_escape($value['notes']) . '</td>') .
							'<td>
								<input type="hidden" name="' . $key .  '" value=' . $value['tracking_id'] . ' />
								<textarea name="' . $value['tracking_id'] . '_UN" maxlength="501" placeholder="'. html_escape($value['user_notes']) . '"></textarea>
							</td>
						</tr>
					</tbody>
					
				</table><br />';
		
	endforeach;
	?>
			<div>
				<input type="submit" value="Submit changes" onclick="return confirm('Submit changes. Are you sure?')" />
				<input type="reset" value="Reset changes" onclick="return(confirm('Undo changes. Are you sure?'))" />
				<span class="group_border">
					<label class="no_own_border" for="math_value">Modify current weights<sup onclick="alert('Modifies the shown values. Don\'t forget to click \'Update weights and notes\' to apply your changes.')" 
						title="Modifies the shown values. Don't forget to click 'Update weights and notes' to apply your changes.">?</sup>: </label>
					<label class="math_operator"><input type="radio" name="operator" value="multiply" checked="checked" /> Multiply</label>
					<label class="math_operator"><input type="radio" name="operator" value="divide" /> Divide</label>
					<label class="math_operator"><input type="radio" name="operator" value="add" /> Add</label>
					<label class="math_operator"><input type="radio" name="operator" value="subtract" /> Subtract</label>
					<span id="to_by_or_not_to_by">by</span>
					<input type="number" name="math_value" id="math_value" value="1" maxlength="4"/>
				</span> 
					
				
			</div>
		</form>
		<?php echo form_open(base_url('split_routine/goto_split')); ?>
			<div>
				<?php if ($manual_view):
					echo 'Manually select a split day: <input type="submit" name="previous" value="Previous split" />
					<input type="submit" name="next" value="Next split" />
					or: <input type="submit" name="planned" value="View planned split" />';
		
				endif; ?>
				
			</div>
			<div>
				
				<?php if (!$manual_view) : 
					echo '<input type="submit" name="selected" value="View manually selected split" />';
				endif; ?>
				
			</div>
			<div class="event_based"><?php if ($condition_buttons) {
				echo 'To make use of the predefined features of this routine (e.g. automatic increasing of weight values), please use these buttons (click only once):<br />';
				echo $condition_buttons;
			} ?></div>
		</form>
	</div>
</div>

<?php
//planned:
//add view current/all exercises
//next/previous day
//adjust?
//table per exercise or entire day in one table?

/* End of file routine.php */
/* Location: .application/controllers/routine.php */