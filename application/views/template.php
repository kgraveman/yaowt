<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title><?php echo (isset($title)?$title:'YAOWT'); ?></title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width">

        <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->

        <link rel="stylesheet" href="<?php echo base_url(); ?>css/normalize.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>css/main.css">
        <script src="<?php echo base_url(); ?>js/vendor/modernizr-2.6.2.min.js"></script>
    </head>
    <body>
        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->
        
        <header><?php $this->load->view('includes/header'); ?></header>
        <!-- <nav><?php $this->load->view('includes/navigation'); ?></nav> -->
        <?php echo $this->session->flashdata('message')?('<div class="notice"><strong>' . $this->session->flashdata('message') . '</strong></div>'):''; ?>
        <?php echo $this->session->flashdata('alert')?('<div class="alert"><strong>' . $this->session->flashdata('alert') . '<strong></div>'):''; ?>
        <?php $vali_errors = validation_errors(); ?>
        <?php echo (!empty($vali_errors)?'<div class="alert">' . validation_errors() . '</div>':''); ?>
        <div id="wrapper">
	        <div id="main" role="main">
	        	<?php if (isset($parser_data)) {
					$this->load->view($content, $parser_data);
				} else {
					
					$this->load->view($content);
				}
				 ?>
	        </div><!--end main-->
        </div> <!-- end wrapper -->
        <footer><?php $this->load->view('includes/footer'); ?></footer>
        
		<?php if ($this->ion_auth->is_admin()) {
			$this->output->enable_profiler(TRUE);
		} ?>
		
        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="<?php echo base_url(); ?>js/vendor/jquery-1.10.1.min.js"><\/script>');</script>
        <script src="<?php echo base_url(); ?>js/plugins.js"></script>
        <script src="<?php echo base_url(); ?>js/main.js"></script>
        
        <script src="//cdnjs.cloudflare.com/ajax/libs/jquery-cookie/1.3.1/jquery.cookie.min.js"></script>
        <script>window.jQuery.cookie || document.write('<script src="<?php echo base_url(); ?>js/jquery.cookie.min.js"><\/script>');</script>
        
        
        <script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>
        <link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/themes/black-tie/jquery-ui.min.css" />
        <script>window.jQuery.ui || document.write('<script src="<?php echo base_url(); ?>js/jquery-ui.min.js"><\/script>\
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/jquery-ui.min.css" />');</script>
        
        
        <script src="//cdn.jsdelivr.net/jquery.ui.timepicker.addon/1.3/jquery-ui-sliderAccess.js"></script>
        <script src="//cdn.jsdelivr.net/jquery.ui.timepicker.addon/1.3/jquery-ui-timepicker-addon.min.js"></script>
        <script>window.jQuery.fn.sliderAccess || document.write('<script src="<?php echo base_url(); ?>js/jquery-ui-sliderAccess.js"><\/script>\
        <script type="text/javascript" src="<?php echo base_url(); ?>js/jquery-ui-timepicker-addon.js"><\/script>');</script>
        
        <!-- <link rel="stylesheet" href="<?php echo base_url(); ?>js/jquery-ui-timepicker-addon.css" /> -->
        <!-- <link rel="stylesheet" href="//cdn.jsdelivr.net/jquery.ui.timepicker.addon/1.3/jquery-ui-timepicker-addon.css" /> -->
		
		<?php if (isset($scripts)) {
			$this->load->view($scripts);
		} ?>
		
		<script type="text/javascript">
		  var _gaq = _gaq || [];
		  _gaq.push(['_setAccount', 'UA-40957093-3']);
		  _gaq.push(['_trackPageview']);
		
		  (function() {
		    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
		    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
		    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
		  })();
	</script>
        
    </body>
</html>
