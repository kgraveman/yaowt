<div id="create_user" class="submain">
	<h1><?php echo lang('register_user_heading');?></h1>
	<p><?php echo lang('register_user_subheading');?></p>
	
	<?php echo (!empty($message)?('<div id="infoMessage">' . $message . '</div>'):''); ?>
	
	<?php echo form_open("auth/user_registration");?>
	
	      <p>
	            <?php echo lang('create_user_username_label', 'username');?> <br />
	            <?php echo form_input($username);?>
	      </p>
	
	      <p>
	            <?php echo lang('create_user_email_label', 'email');?> <br />
	            <?php echo form_input($email);?>
	      </p>
	
	      <p>
	            <?php echo lang('create_user_password_label', 'password');?> <br />
	            <?php echo form_input($password);?>
	      </p>
	
	      <p>
	            <?php echo lang('create_user_password_confirm_label', 'password_confirm');?> <br />
	            <?php echo form_input($password_confirm);?>
	      </p>
	
			<p>
				<?php //TODO LEGAL: links; ?>
				By signing up you agree to our <a href="http://graveman.co/ciproject/legal/terms" target="_blank">terms of service</a> and <a href="http://graveman.co/ciproject/legal/privacy" target="_blank">privacy policy</a>. 
				Your IP is collected for functional and analytical purposes.
				Lastly, when your account was created and your last login are recorded as timestamps.
			</p>
	
	
	      <p><?php echo form_submit('submit', lang('create_user_submit_btn'));?></p>
	
	<?php echo form_close();?>
	
	
	<?php
	/*      <p>
	            <?php echo lang('create_user_fname_label', 'first_name');?> <br />
	            <?php echo form_input($first_name);?>
	      </p>
	
	      <p>
	            <?php echo lang('create_user_lname_label', 'first_name');?> <br />
	            <?php echo form_input($last_name);?>
	      </p>
	
	      <p>
	            <?php echo lang('create_user_company_label', 'company');?> <br />
	            <?php echo form_input($company);?>
	      </p>
	
	      <p>
	            <?php echo lang('create_user_email_label', 'email');?> <br />
	            <?php echo form_input($email);?>
	      </p>
	
	      <p>
	            <?php echo lang('create_user_phone_label', 'phone');?> <br />
	            <?php echo form_input($phone);?>
	      </p>
	
	      <p>
	            <?php echo lang('create_user_password_label', 'password');?> <br />
	            <?php echo form_input($password);?>
	      </p>
	
	      <p>
	            <?php echo lang('create_user_password_confirm_label', 'password_confirm');?> <br />
	            <?php echo form_input($password_confirm);?>
	      </p>
	
	
	      <p><?php echo form_submit('submit', lang('create_user_submit_btn'));?></p>
	
	<?php echo form_close();?>
	*/ ?>
</div>