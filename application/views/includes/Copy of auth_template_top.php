<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title><?php echo $title ?></title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width">
        <!--TODO dont have css here -->
		<style type="text/css"> 
			body{padding: 0; margin: 0;}
		</style>
        <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->

        <link rel="stylesheet" href="<?php echo base_url(); ?>css/normalize.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>css/main.css">
        <script src="<?php echo base_url(); ?>js/vendor/modernizr-2.6.2.min.js"></script>
    </head>
    <body>
        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->
        
        <header><?php $this->load->view('includes/header'); ?></header>
        <nav><?php $this->load->view('includes/navigation'); ?></nav>
	        <?php //echo $this->session->flashdata('message')?('<div class="notice">' . $this->session->flashdata('message') . '</div>'):''; ?>
	        <?php //echo $this->session->flashdata('alert')?('<div class="alert">' . $this->session->flashdata('alert') . '</div>'):''; ?>
        
        <?php //$key_passer =  $this->session->flashdata('csrfkey'); ?> <!-- TODO is this a leak? -->
        <?php //$this->session->set_flashdata('csrfkey', $key_passer);  ?>
        
        <div id="main" role="main">